import React, { PropTypes } from 'react';

export default class PersonalDetailsForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      surname: '',
      country: 'Russian federation',
      firstDate: '',
      secondDate: '',
      issuer: '',
    };

    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeSurname = this.handleChangeSurname.bind(this);
    this.handleChangeCountry = this.handleChangeCountry.bind(this);
    this.handleChangeFirstDate = this.handleChangeFirstDate.bind(this);
    this.handleChangeSecondDate = this.handleChangeSecondDate.bind(this);
    this.handleChangeIssuer = this.handleChangeIssuer.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
    handleChangeName(e) {
      this.setState({
        name: e.target.value
      })
    };
    handleChangeSurname(e) {
      this.setState({
        surname: e.target.value
      })
    };
    handleChangeFirstDate(e) {
      this.setState({
        firstDate: e.target.value
      })
    };
    handleChangeSecondDate(e) {
      this.setState({
        secondDate: e.target.value
      })
    };
    handleChangeCountry(e) {
      this.setState({
        country: e.target.value
      })
    }
    handleChangeIssuer(e) {
      this.setState({
        issuer: e.target.value
      })
    }
    handleSubmit(e) {}

    render() {
      const verify = this.props.data.verify;
      const values = ["Russian federation", "USA", "Portugal"];
        return (
              <form onSubmit={this.handleSubmit} action="../../../src/AppBundle/handler.php" method="post">
              <div id="centre-column">
                  <div id="centre-column-h" className="centre-column-h-settings">PERSONAL DETAILS
                      <span className={`centre-column-h-extra ${verify ? 'verify' : ''}`}>{verify ? 'Verified' : 'Not verified'}</span>
                  </div>
                  <div id="application-form2" className="settings-padding">
                      <div className="form2-h mobile-padding">First name(s)</div>
                      <div className="form2-d2 mobile-padding font14">
                          <div className="form-div grey-borders">
                              <input required type="text" name="first_name" onChange={this.handleChangeName} value={this.state.name} placeholder="Your name"/>
                          </div>
                      </div>
                      <div className="form2-h mobile-padding">Second name(s)</div>
                      <div className="form2-d2 mobile-padding font14">
                          <div className="form-div grey-borders">
                              <input required type="text" name="second_name" onChange={this.handleChangeSurname} value={this.state.surname} placeholder="Your surname"/>
                          </div>
                      </div>

                      <div className="form2-s mobile-padding">PASSPORT OR ID</div>

                      <div className="form2-h mobile-padding">Issuing country</div>
                      <div className="form2-d2 mobile-padding">
                          <div className="form-div2 select-green3-wrapper height32">
                              <select required value={this.state.country} onChange={this.handleChangeCountry} name="country" className="select-green" style={{ width: '100%' }}>
                                  {values.map(e => (<option key={e} value={e}>{e}</option>))}
                              </select>
                          </div>
                      </div>

                      <div className="form2-2date mobile-padding">
                          <div className="form2-col">
                              <div className="form2-h">Issue date</div>
                              <div className="form2-d2">
                                  <div className="datepicker datepicker-white-bg">
                                      <input required type="date" name="issue_date" onChange={this.handleChangeFirstDate} value={this.state.firstDate} className="date" placeholder="day.month.year"/>
                                  </div>
                              </div>
                          </div>
                          <div className="form2-col">
                              <div className="form2-h">Valid till</div>
                              <div className="form2-d2">
                                  <div className="datepicker datepicker-white-bg">
                                      <input required type="date" name="valid_till" onChange={this.handleChangeSecondDate} value={this.state.secondDate} className="date" placeholder="day.month.year" />
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div className="form2-h mobile-padding">Issued by</div>
                      <div className="form2-d2 mobile-padding font14">
                          <div className="form-div grey-borders">
                              <input required type="text" name="issued_by" value={this.state.issuer} onChange={this.handleChangeIssuer} placeholder="Your manager's name"/>
                          </div>
                      </div>
                  </div>


                  <div className="form2-h mobile-padding settings-padding">Please upload your scanned documents
                      and sent to verify
                  </div>
                  <div className="attach-file-div2 mobile-padding settings-padding">
                      <a href="#" className="attach-file-link">
                          <span>Attach file</span>
                      </a>
                      <div className="uploaded-file-div">
                          <div className="uploaded-item uploaded-item-type1">File1<span className="ext">.docx</span><a
                                      href="#" title="delete" className="delete-uploaded"></a></div>
                          <div className="uploaded-item uploaded-item-type2">Document docum...<span className="ext">.docx</span><a
                                      href="#" title="delete" className="delete-uploaded"></a></div>
                          <div className="uploaded-item uploaded-item-type3">Document docum...<span className="ext">.docx</span><a
                                      href="#" title="delete" className="delete-uploaded"></a></div>
                      </div>
                      <div className="clear-both"></div>
                  </div>
                  <input id="settings-personal-details-submit" type="submit" value="Submit" className="green-button8 confirm-button4" />
              </div>
          </form>
        );
    }
}
