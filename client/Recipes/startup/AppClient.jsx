import ReactDOM from 'react-dom';
import React from 'react';
import PersonalDetailsForm from '../../../client/Recipes/components/PersonalDetailsForm.jsx'
//
// const routes = (
//     <div>
//         // set a base url to '/' or maybe '/app_dev.php'
//         <Route path={window.baseUrl} component={Recipes}></Route>
//         <Route path={window.baseUrl+'recipe/:slug'} component={Recipe}/>
//     </div>
// );
export default () => {
  const props = {
    verify: true,
  };
  return (
    <div>
      <PersonalDetailsForm data = {props}/>
    </div>
  );
}



// export default (props) => {
//     var rprops = {};
//     rprops.params = props;
//     var createElement = function(Component, compProps) {
//         for (var prop in props) { compProps.params[prop] = props[prop]; }
//         return <Component {...compProps} />
//     };
//     return (
//         <Router createElement={createElement} history={browserHistory} children={routes} {...props}/>
//     );
// };
