<?php
// src/AppBundle/Form/MessageType.php
namespace AppBundle\Form;

use AppBundle\Entity\MessageType as MT;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', EntityType::class, array(
            'class' => 'AppBundle:MessageType',
             'required' => false,
             'choice_label' => 'name',
            'attr'=>array('class'=>'form-td-data form-div grey-borders'),
            'label_attr'=>array('class'=>'form-td-data'),
         //   'query_builder' => function(MyEntityRepository $repository) { 
      //          return $repository->findAll();
     //       }, 
        ))
            ->add('datetime', DateType::class, array('attr'=>array('class'=>'form-td-data form-div grey-borders'),'label_attr'=>array('class'=>'form-td-data'),))
            ->add('author', EntityType::class, array(
            'class' => 'AppBundle:User',
            'choice_label' => 'username',
             'attr'=>array('class'=>'form-td-data form-div grey-borders'),
            'label_attr'=>array('class'=>'form-td-data'),
        ))
            ->add('target_user',  EntityType::class, array(
            'class' =>'AppBundle:User',
            'choice_label' => 'username',
            'attr'=>array('class'=>'form-td-data form-div grey-borders'),
            'label_attr'=>array('class'=>'form-td-data'),
        ))
            ->add('target_holder' ,  EntityType::class, array(
          'class' => 'AppBundle:User',
            'choice_label' => 'name',
            'attr'=>array('class'=>'form-td-data form-div grey-borders'),
            'label_attr'=>array('class'=>'form-td-data'),
        ))
            ->add('subject', TextType::class, array('attr'=>array('class'=>'form-td-data form-div grey-borders'),'label_attr'=>array('class'=>'form-td-data'),))
            ->add('text', TextareaType::class, array('attr'=>array('class'=>'form-td-data form-div grey-borders'),'label_attr'=>array('class'=>'form-td-data'),))
            ->add('save', SubmitType::class, array('label' => 'Отправить','attr'=>array('class'=>'form-div grey-borders'),
            )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Message',
        ));
    }
}
