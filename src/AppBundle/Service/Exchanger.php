<?php
namespace AppBundle\Service;

use AppBundle\Service\Rate;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraints\DateTime;

class Exchanger
{
    /** @var  Registry */
    protected $doctrine;

    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
//        echo $doctrine->getRepository('AppBundle:Currency')->find(1)->getName();
    }

    public function getRateByName($from, $to)
    {
        $rep = $this->doctrine->getRepository('AppBundle:Currency');
        $f = $rep->find($from)->getName();
        $t = $rep->find($to)->getName();

        return $this->getRate($f, $t);
    }

    /**
     * @param integer $from
     * @param integer $to
     * @return \AppBundle\Entity\Rate|\AppBundle\Service\Rate
     */
    public function getRateID($from, $to)
    {
        $em = $this->doctrine->getManager();
        $rep = $em->getRepository('AppBundle:Rate');
        /** @var \AppBundle\Entity\Rate $res */
        $res = $rep->findOneBy([
            'from' => $from,
            'to' => $to
        ]);

        if ($res) {
            $date = new \DateTime();
            $diff =  $date->getTimestamp() - $res->getTimeLastUpdate()->getTimestamp();

            if ($diff > 5*60) {
                $rep = $em->getRepository('AppBundle:Currency');
                $f = $rep->find($from);
                $t = $rep->find($to);

                $rate = $this->fetchRate1($f->getShortName(), $t->getShortName());

                $res->setTimeLastUpdate($date);
                //$res->setAsk($rate->ask);
                //$res->setBid($rate->bid);
                $res->setRate($rate);
                $em->flush();

                return $res;
            } else {
                return $res;
            }
        } else {
            $rep = $em->getRepository('AppBundle:Currency');
            $f = $rep->find($from);
            $t = $rep->find($to);
            $rate = $this->fetchRate1($f->getShortName(), $t->getShortName());

            $e_rate = new \AppBundle\Entity\Rate();
            $e_rate->setFrom($f);
            $e_rate->setTo($t);
            //$e_rate->setAsk($rate->ask);
            //$e_rate->setBid($rate->bid);
            $e_rate->setRate($rate);
            $e_rate->setTimeLastUpdate(new \DateTime());
            $e_rate->setMargin(0.98);

            $em->persist($e_rate);
            $em->flush();
            return $e_rate;
        }
    }

    //TODO: will be many currency protocols - redo as interface with injectors
    private function fetchRate($from, $to)
    {
        $request = 'select * from yahoo.finance.xchange where pair = \'' . $from . $to . '\'';
        $server = 'http://query.yahooapis.com/v1/public/yql';
        $request_params = [
            'q' => $request,
            'format' => 'json',
            'env' => 'store://datatables.org/alltableswithkeys'
        ];

        $result_str = file_get_contents($server . '?' . http_build_query($request_params));
        $result = json_decode($result_str);

        return new Rate($result->query->results->rate);
    }
    private function fetchRate1($from, $to)
    {
        $server = 'https://www.apilayer.net/api/';
        $endpoint = 'live';
        $key = '5dd0b5bbfb321599f7b3e2cd2c44765d';
        $request_params = [
            'access_key' => $key,
            'source' => $from,
            'currencies' => $to
            //'format' => 1 //improves readability, not needed
        ];
        $ch = curl_init($server . $endpoint . '?' . http_build_query($request_params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);
        curl_close($ch);
        $exchangeRates = json_decode($json, true);
        return $exchangeRates['quotes'][$from.$to];
    }
}