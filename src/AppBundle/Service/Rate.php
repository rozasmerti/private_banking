<?php

namespace AppBundle\Service;


class Rate
{
    public $id;
    public $name;
    public $rate;
    public $ask;
    public $bid;
    public $from;
    public $to;

    public function __construct($rate)
    {
        $this->id = $rate->id;
        $this->name = $rate->Name;
        $this->rate = $rate->Rate;
        $this->ask = $rate->Ask;
        $this->bid = $rate->Bid;

        $arr = explode('/', $rate->Name);
        $this->from = $arr[0];
        $this->to = $arr[1];
    }
}