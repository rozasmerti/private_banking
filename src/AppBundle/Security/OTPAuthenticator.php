<?php
namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class OTPAuthenticator extends AbstractGuardAuthenticator
{

    public function getCredentials(Request $request)
    {

        if (!$pin = $request->request->get('pin')
            && !$client_id = $request->request->get('client_id')
        ) {
            return null;
        }

        #!$token = $request->headers->get('X-AUTH-OTP');

        return array(
            'pin' => $pin,
            'client_id' => $client_id
        );
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $client_id = $credentials['client_id'];
        //get user from session
        #$user =

        // if a User object, checkCredentials() is called
        return $userProvider->loadUserByUsername($client_id);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        // check credentials - e.g. make sure the password is valid
        // no credential check is needed in this case
        if ($user->getUsername() == 'dino') {
            return true;
        }
        // return true to cause authentication success
        return false;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = array(
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        );

        return new JsonResponse($data, 403);
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            // you might translate this message
            'message' => 'Authentication Required'
        );

        return new JsonResponse($data, 401);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}