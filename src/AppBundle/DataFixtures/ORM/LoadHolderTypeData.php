<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\HolderType;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadHolderTypeData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $type = new HolderType();
        $type->setName('private');
        $type->setDescription('Holder is private person');
        $manager->persist($type);

        $type = new HolderType();
        $type->setName('business');
        $type->setDescription('Holder is juridical person');
        $manager->persist($type);

        $manager->flush();
    }
}