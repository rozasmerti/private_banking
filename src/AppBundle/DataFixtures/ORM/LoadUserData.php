<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function load(ObjectManager $manager)
    {
        $manager->persist($this->createUser([
            'id' => 0,
            'username' => 'admin',
            'password' => 'admin',
            'email' => 'admin@admin.adm',
            'token' => 'admin_token',
            'name' => 'Admin',
            'surname' => 'AdminMan',
//            'time' => new \DateTime(),
            'is_active' => false,
            'is_enabled' => false,
            'is_locked' => false
        ]));

        $manager->persist($this->createUser([
            'id' => 1,
            'username' => '001001',
            'password' => 'testtest',
            'email' => 'test@test.tst',
            'token' => 'test_token',
            'name' => 'Test',
            'surname' => 'Tester',
            'is_active' => true,
            'is_enabled' => true,
            'is_locked' => false
        ]));

        $manager->persist($this->createUser([
            'id' => 2,
            'username' => '001002',
            'password' => 'testtest2',
            'email' => 'test2@test.tst',
            'token' => 'test_token',
            'name' => 'TestWithPartial',
            'surname' => 'Tester',
            'is_active' => true,
            'is_enabled' => true,
            'is_locked' => false
        ]));
        $manager->persist($this->createUser([
            'id' => 3,
            'username' => '002002',
            'password' => 'test_manager',
            'email' => 'manager@manager.mgr',
            'token' => 'manager_token',
            'name' => 'Manager',
            'surname' => 'Management',
            'is_active' => true,
            'is_enabled' => true,
            'is_locked' => false
        ]));

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function createUser($user_data)
    {
        $user = new User();

        if (isset($user_data['id'])) {
            $user->setId($user_data['id']);
        }
        if (isset($user_data['username'])) {
            $user->setUsername($user_data['username']);
        }
        if (isset($user_data['password'])) {
            $encoder = $this->container->get('security.password_encoder');
            $password = $encoder->encodePassword($user, $user_data['password']);
            $user->setPassword($password);
        }
        if (isset($user_data['email'])) {
            $user->setEmail($user_data['email']);
        }
        if (isset($user_data['token'])) {
            $user->setToken($user_data['token']);
        } else {
            $user->setToken(uniqid(null));
        }
        if (isset($user_data['name'])) {
            $user->setName($user_data['name']);
        }
        if (isset($user_data['surname'])) {
            $user->setSurname($user_data['surname']);
        }
        if (isset($user_data['time'])) {
            $user->setTimeCreated($user_data['time']);
        } else {
            $user->setTimeCreated(new \DateTime());
        }
        if (isset($user_data['is_active'])) {
            $user->setIsActive($user_data['is_active']);
        }
        if (isset($user_data['is_enabled'])) {
            $user->setIsEnabled($user_data['is_enabled']);
        }
        if (isset($user_data['is_locked'])) {
            $user->setIsLocked($user_data['is_locked']);
        }

        return $user;
    }
}