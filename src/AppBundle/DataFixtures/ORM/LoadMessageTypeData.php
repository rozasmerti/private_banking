<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\MessageType;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadMessageTypeData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $type = new MessageType();
        $type->setName('notification');
        $type->setComment('Notification for holder (or user)');
        $manager->persist($type);

        $type = new MessageType();
        $type->setName('news');
        $type->setComment('News are global and can be read by any bank client');
        $manager->persist($type);

        $type = new MessageType();
        $type->setName('message');
        $type->setComment('Messages are private only between user and bank authorized personal and');
        $manager->persist($type);

        $manager->flush();
    }
}