<?php
/**
 * Created by PhpStorm.
 * User: Dinozor
 * Date: 11/16/2016
 * Time: 17:49
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\TransferType;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTransferTypeData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $type = new TransferType();
        $type->setName('own');
        $type->setDescription('Transfer between own accounts');

        $type = new TransferType();
        $type->setName('local');
        $type->setDescription('Transfer between local bank accounts');

        $type = new TransferType();
        $type->setName('international');
        $type->setDescription('International transfers');

        $manager->persist($type);
        $manager->flush();
    }
}