<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="transfers")
 */
class Transfer
{
    public function __construct()
    {
        $this->time_created = new \DateTime();
        $this->time_last_edited = new \DateTime();
        $this->fee = 0.0;
    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_created", type="datetime", options={"default": 0})
     */
    private $time_created;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_last_edited", type="datetime", options={"default": 0})
     */
    private $time_last_edited;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_finished", type="datetime", options={"default": 0}, nullable=true)
     */
    private $time_finished;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="initiator", referencedColumnName="id")
     *
     * @var User
     */
    private $initiator;

    /**
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     *
     */
    private $from;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $to_account;

    /**
     * @var string
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    private $to_name;

    /**
     * @ORM\Column(type="string", length=11, nullable=true)
     */
    private $swift;
    /**
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_schedule", type="datetime", options={"default": 0})
     */
    private $time_schedule;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=15, scale=2)
     */
    private $amount;
    /**
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    private $currency;

    /**
     * @ORM\ManyToOne(targetEntity="ShipmentType")
     * @ORM\JoinColumn(name="shipment_type_id", referencedColumnName="id")
     */
    private $shipment; //economy, normal, express

    /**
     * @ORM\ManyToOne(targetEntity="TransactionStatus")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    private $status;
    /**
     * @ORM\ManyToOne(targetEntity="TransferType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type; //between
    /**
     * @var string
     *
     * @ORM\Column(name="fee", type="decimal", precision=15, scale=2, options={"default": 0})
     */
    private $fee;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="manager", referencedColumnName="id", nullable=true)
     */
    private $manager;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timeCreated
     *
     * @param \DateTime $timeCreated
     *
     * @return Transfer
     */
    public function setTimeCreated($timeCreated)
    {
        $this->time_created = $timeCreated;

        return $this;
    }

    /**
     * Get timeCreated
     *
     * @return \DateTime
     */
    public function getTimeCreated()
    {
        return $this->time_created;
    }

    /**
     * Set timeLastEdited
     *
     * @param \DateTime $timeLastEdited
     *
     * @return Transfer
     */
    public function setTimeLastEdited($timeLastEdited)
    {
        $this->time_last_edited = $timeLastEdited;

        return $this;
    }

    /**
     * Get timeLastEdited
     *
     * @return \DateTime
     */
    public function getTimeLastEdited()
    {
        return $this->time_last_edited;
    }

    /**
     * Set timeFinished
     *
     * @param \DateTime $timeFinished
     *
     * @return Transfer
     */
    public function setTimeFinished($timeFinished)
    {
        $this->time_finished = $timeFinished;

        return $this;
    }

    /**
     * Get timeFinished
     *
     * @return \DateTime
     */
    public function getTimeFinished()
    {
        return $this->time_finished;
    }

    /**
     * Set toAccount
     *
     * @param string $toAccount
     *
     * @return Transfer
     */
    public function setToAccount($toAccount)
    {
        $this->to_account = $toAccount;

        return $this;
    }

    /**
     * Get toAccount
     *
     * @return string
     */
    public function getToAccount()
    {
        return $this->to_account;
    }

    /**
     * Set swift
     *
     * @param string $swift
     *
     * @return Transfer
     */
    public function setSwift($swift)
    {
        $this->swift = $swift;

        return $this;
    }

    /**
     * Get swift
     *
     * @return string
     */
    public function getSwift()
    {
        return $this->swift;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Transfer
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set timeSchedule
     *
     * @param \DateTime $timeSchedule
     *
     * @return Transfer
     */
    public function setTimeSchedule($timeSchedule)
    {
        $this->time_schedule = $timeSchedule;

        return $this;
    }

    /**
     * Get timeSchedule
     *
     * @return \DateTime
     */
    public function getTimeSchedule()
    {
        return $this->time_schedule;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Transfer
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return Transfer
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set fee
     *
     * @param string $fee
     *
     * @return Transfer
     */
    public function setFee($fee)
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * Get fee
     *
     * @return string
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * Set initiator
     *
     * @param \AppBundle\Entity\User $initiator
     *
     * @return Transfer
     */
    public function setInitiator(\AppBundle\Entity\User $initiator = null)
    {
        $this->initiator = $initiator;

        return $this;
    }

    /**
     * Get initiator
     *
     * @return \AppBundle\Entity\User
     */
    public function getInitiator()
    {
        return $this->initiator;
    }

    /**
     * Set from
     *
     * @param \AppBundle\Entity\Account $from
     *
     * @return Transfer
     */
    public function setFrom(\AppBundle\Entity\Account $from = null)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return \AppBundle\Entity\Account
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set currency
     *
     * @param \AppBundle\Entity\Currency $currency
     *
     * @return Transfer
     */
    public function setCurrency(\AppBundle\Entity\Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \AppBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set shipment
     *
     * @param \AppBundle\Entity\ShipmentType $shipment
     *
     * @return Transfer
     */
    public function setShipment(\AppBundle\Entity\ShipmentType $shipment = null)
    {
        $this->shipment = $shipment;

        return $this;
    }

    /**
     * Get shipment
     *
     * @return \AppBundle\Entity\ShipmentType
     */
    public function getShipment()
    {
        return $this->shipment;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\TransactionStatus $status
     *
     * @return Transfer
     */
    public function setStatus(\AppBundle\Entity\TransactionStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\TransactionStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\TransferType $type
     *
     * @return Transfer
     */
    public function setType(\AppBundle\Entity\TransferType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\TransferType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set manager
     *
     * @param \AppBundle\Entity\User $manager
     *
     * @return Transfer
     */
    public function setManager(\AppBundle\Entity\User $manager = null)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get manager
     *
     * @return \AppBundle\Entity\User
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Set toName
     *
     * @param string $toName
     *
     * @return Transfer
     */
    public function setToName($toName)
    {
        $this->to_name = $toName;

        return $this;
    }

    /**
     * Get toName
     *
     * @return string
     */
    public function getToName()
    {
        return $this->to_name;
    }
}
