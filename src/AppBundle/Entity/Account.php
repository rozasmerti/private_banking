<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="accounts")
 */
class Account
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true, length=21)
     */
    private $number;// = 'LC11BNKO5555555555540';

    /**
     * @ORM\ManyToOne(targetEntity="Holder")
     * @ORM\JoinColumn(name="primary_holder_id", referencedColumnName="id")
     */
    private $primary_holder;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $is_active;

    /**
     * @ORM\Column(name="is_blocked", type="boolean")
     */
    private $is_blocked;

    /**
     * @ORM\Column(type="datetime", name="time_created")
     */
    private $time_created;

    /**
     * @ORM\Column(type="datetime", name="valid_until")
     */
    private $valid_until;

    /**
     * @ORM\OneToMany(targetEntity="AccountCurrency", mappedBy="account")
     */
    private $currencies;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Account
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Account
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set isBlocked
     *
     * @param boolean $isBlocked
     *
     * @return Account
     */
    public function setIsBlocked($isBlocked)
    {
        $this->is_blocked = $isBlocked;

        return $this;
    }

    /**
     * Get isBlocked
     *
     * @return boolean
     */
    public function getIsBlocked()
    {
        return $this->is_blocked;
    }

    /**
     * Set timeCreated
     *
     * @param \DateTime $timeCreated
     *
     * @return Account
     */
    public function setTimeCreated($timeCreated)
    {
        $this->time_created = $timeCreated;

        return $this;
    }

    /**
     * Get timeCreated
     *
     * @return \DateTime
     */
    public function getTimeCreated()
    {
        return $this->time_created;
    }

    /**
     * Set validUntil
     *
     * @param \DateTime $validUntil
     *
     * @return Account
     */
    public function setValidUntil($validUntil)
    {
        $this->valid_until = $validUntil;

        return $this;
    }

    /**
     * Get validUntil
     *
     * @return \DateTime
     */
    public function getValidUntil()
    {
        return $this->valid_until;
    }

    /**
     * Set primaryHolderId
     *
     * @param \AppBundle\Entity\Holder $primaryHolderId
     *
     * @return Account
     */
    public function setPrimaryHolder(\AppBundle\Entity\Holder $primaryHolderId = null)
    {
        $this->primary_holder = $primaryHolderId;

        return $this;
    }

    /**
     * Get primaryHolderId
     *
     * @return \AppBundle\Entity\Holder
     */
    public function getPrimaryHolder()
    {
        return $this->primary_holder;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->currencies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add currency
     *
     * @param \AppBundle\Entity\AccountCurrency $currency
     *
     * @return Account
     */
    public function addCurrency(\AppBundle\Entity\AccountCurrency $currency)
    {
        $this->currencies[] = $currency;

        return $this;
    }

    /**
     * Remove currency
     *
     * @param \AppBundle\Entity\AccountCurrency $currency
     */
    public function removeCurrency(\AppBundle\Entity\AccountCurrency $currency)
    {
        $this->currencies->removeElement($currency);
    }

    /**
     * Get currencies
     *
     * @return  \Doctrine\Common\Collections\Collection
     */
    public function getCurrencies()
    {
        return $this->currencies;
    }
}
