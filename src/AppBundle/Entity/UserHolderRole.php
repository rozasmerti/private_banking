<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_holder_role")
 */
class UserHolderRole
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="holders_and_roles")
     * @ORM\JoinColumn(name="user_id")
     */
    private $user;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Holder")
     * @ORM\JoinColumn(name="holder_id")
     */
    private $holder;

    /**
     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumn(name="role_id")
     */
    private $role;

    /**
     * @ORM\Column(type="datetime", name="time_created")
     */
    private $time_created;

    /**
     * @ORM\Column(type="datetime", name="valid_until")
     */
    private $valid_till;

    /**
     * @ORM\Column(type="boolean", name="enabled")
     */
    private $enabled;
    //private $comment_id;
    

    /**
     * Set user
     *
     * @param string $user
     *
     * @return UserHolderRole
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return UserHolderRole
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set timeCreated
     *
     * @param \DateTime $timeCreated
     *
     * @return UserHolderRole
     */
    public function setTimeCreated($timeCreated)
    {
        $this->time_created = $timeCreated;

        return $this;
    }

    /**
     * Get timeCreated
     *
     * @return \DateTime
     */
    public function getTimeCreated()
    {
        return $this->time_created;
    }

    /**
     * Set validTill
     *
     * @param \DateTime $validTill
     *
     * @return UserHolderRole
     */
    public function setValidTill($validTill)
    {
        $this->valid_till = $validTill;

        return $this;
    }

    /**
     * Get validTill
     *
     * @return \DateTime
     */
    public function getValidTill()
    {
        return $this->valid_till;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return UserHolderRole
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set holder
     *
     * @param \AppBundle\Entity\Holder $holder
     *
     * @return UserHolderRole
     */
    public function setHolder(\AppBundle\Entity\Holder $holder = null)
    {
        $this->holder = $holder;

        return $this;
    }

    /**
     * Get holder
     *
     * @return \AppBundle\Entity\Holder
     */
    public function getHolder()
    {
        return $this->holder;
    }
}
