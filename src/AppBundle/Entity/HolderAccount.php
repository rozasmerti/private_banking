<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="holder_accounts")
 */
class HolderAccount
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Holder", inversedBy="id")
     */
    private $holder;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="id")
     */
    private $account;
}