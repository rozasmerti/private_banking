<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


// TODO: split detail by 4.5 lines (columns) - (Who (reg.nr), Bank, Swift, Dest? (some code/comment)
/**
 * Class Transaction
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TransactionRepository")
 * @ORM\Table(name="transactions")
 */
class Transaction
{

    public function __construct()
    {
        $this->time_created = new \DateTime();
        $this->time_last_status = new \DateTime();
    }

    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    private $account;

    /**
     * @var TransactionStatus
     *
     * @ORM\ManyToOne(targetEntity="TransactionStatus")
     * @ORM\JoinColumn(name="status", referencedColumnName="id")
     */
    private $status;

    /**
     * @var TransactionType
     *
     * @ORM\ManyToOne(targetEntity="TransactionType")
     * @ORM\JoinColumn(name="type", referencedColumnName="id")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=128)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="details", type="text")
     */
    private $details;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=15, scale=2)
     */
    private $amount;

    // TODO: replace all foreigns to key_ID (amount_currency_id)
    /**
     * @var Currency
     *
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(name="amount_currency", referencedColumnName="id")
     */
    private $amount_currency;

    /**
     * @var string
     *
     * @ORM\Column(name="balance", type="decimal", precision=15, scale=2)
     */
    private $balance;
    /**
     * @var Currency
     *
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(name="balance_currency", referencedColumnName="id")
     */
    private $balance_currency;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_created", type="datetime", options={"default": 0})
     */
    private $time_created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_last_status", type="datetime", options={"default": 0})
     */
    private $time_last_status;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Transaction
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set details
     *
     * @param string $details
     *
     * @return Transaction
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return Transaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set balance
     *
     * @param string $balance
     *
     * @return Transaction
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return string
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set timeCreated
     *
     * @param \DateTime $timeCreated
     *
     * @return Transaction
     */
    public function setTimeCreated($timeCreated)
    {
        $this->time_created = $timeCreated;

        return $this;
    }

    /**
     * Get timeCreated
     *
     * @return \DateTime
     */
    public function getTimeCreated()
    {
        return $this->time_created;
    }

    /**
     * Set timeLastStatus
     *
     * @param \DateTime $timeLastStatus
     *
     * @return Transaction
     */
    public function setTimeLastStatus($timeLastStatus)
    {
        $this->time_last_status = $timeLastStatus;

        return $this;
    }

    /**
     * Get timeLastStatus
     *
     * @return \DateTime
     */
    public function getTimeLastStatus()
    {
        return $this->time_last_status;
    }

    /**
     * Set account
     *
     * @param \AppBundle\Entity\Account $account
     *
     * @return Transaction
     */
    public function setAccount(\AppBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\TransactionStatus $status
     *
     * @return Transaction
     */
    public function setStatus(\AppBundle\Entity\TransactionStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\TransactionStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\TransactionType $type
     *
     * @return Transaction
     */
    public function setType(\AppBundle\Entity\TransactionType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\TransactionType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amountCurrency
     *
     * @param \AppBundle\Entity\Currency $amountCurrency
     *
     * @return Transaction
     */
    public function setAmountCurrency(\AppBundle\Entity\Currency $amountCurrency = null)
    {
        $this->amount_currency = $amountCurrency;

        return $this;
    }

    /**
     * Get amountCurrency
     *
     * @return \AppBundle\Entity\Currency
     */
    public function getAmountCurrency()
    {
        return $this->amount_currency;
    }

    /**
     * Set balanceCurrency
     *
     * @param \AppBundle\Entity\Currency $balanceCurrency
     *
     * @return Transaction
     */
    public function setBalanceCurrency(\AppBundle\Entity\Currency $balanceCurrency = null)
    {
        $this->balance_currency = $balanceCurrency;

        return $this;
    }

    /**
     * Get balanceCurrency
     *
     * @return \AppBundle\Entity\Currency
     */
    public function getBalanceCurrency()
    {
        return $this->balance_currency;
    }
}
