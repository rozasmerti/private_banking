<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="rates")
 */
class Rate
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(name="from_id", referencedColumnName="id")
     */
    private $from;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(name="to_id", referencedColumnName="id")
     */
    private $to;

    /**
     * @var float
     * @ORM\Column(name="rate", type="decimal", precision=15, scale=8)
     */
    private $rate;

    /**
     * @var float
     * @ORM\Column(name="ask", type="decimal", precision=15, scale=8, nullable=true)
     */
    private $ask;

    /**
     * @var float
     * @ORM\Column(name="bid", type="decimal", precision=15, scale=8, nullable=true)
     */
    private $bid;

    /**
     * @var float
     * @ORM\Column(name="margin", type="decimal", precision=15, scale=8)
     */
    private $margin;

    /**
     * @var \DateTime
     * @ORM\Column(name="time_last_update", type="datetime")
     */
    private $time_last_update;

    /**
     * Set rate
     *
     * @param string $rate
     *
     * @return Rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return string
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set ask
     *
     * @param string $ask
     *
     * @return Rate
     */
    public function setAsk($ask)
    {
        $this->ask = $ask;

        return $this;
    }

    /**
     * Get ask
     *
     * @return string
     */
    public function getAsk()
    {
        return $this->ask;
    }

    /**
     * Set bid
     *
     * @param string $bid
     *
     * @return Rate
     */
    public function setBid($bid)
    {
        $this->bid = $bid;

        return $this;
    }

    /**
     * Get bid
     *
     * @return string
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set margin
     *
     * @param string $margin
     *
     * @return Rate
     */
    public function setMargin($margin)
    {
        $this->margin = $margin;

        return $this;
    }

    /**
     * Get margin
     *
     * @return string
     */
    public function getMargin()
    {
        return $this->margin;
    }

    /**
     * Set timeLastUpdate
     *
     * @param \DateTime $timeLastUpdate
     *
     * @return Rate
     */
    public function setTimeLastUpdate($timeLastUpdate)
    {
        $this->time_last_update = $timeLastUpdate;

        return $this;
    }

    /**
     * Get timeLastUpdate
     *
     * @return \DateTime
     */
    public function getTimeLastUpdate()
    {
        return $this->time_last_update;
    }

    /**
     * Set from
     *
     * @param \AppBundle\Entity\Currency $from
     *
     * @return Rate
     */
    public function setFrom(\AppBundle\Entity\Currency $from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return \AppBundle\Entity\Currency
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set to
     *
     * @param \AppBundle\Entity\Currency $to
     *
     * @return Rate
     */
    public function setTo(\AppBundle\Entity\Currency $to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return \AppBundle\Entity\Currency
     */
    public function getTo()
    {
        return $this->to;
    }
}
