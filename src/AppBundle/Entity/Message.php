<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Message
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\MessageRepository")
 * @ORM\Table(name="messages")
 */
class Message
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="MessageType")
     * @ORM\JoinColumn(name="type", referencedColumnName="id")
     */
    private $type;

    /**
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="author", referencedColumnName="id")
     *
     * @var User
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="target_user", referencedColumnName="id")
     *
     * @var User
     */
    private $target_user;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="target_holder", referencedColumnName="id")
     *
     * @var Holder
     */
    private $target_holder;

    /**
     * @ORM\Column(name="subject", type="string", length=127)
     */
    private $subject;
    /**
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @ORM\Column(name="is_read", type="boolean")
     */
    private $is_read;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return Message
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return Message
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Message
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set isRead
     *
     * @param boolean $isRead
     *
     * @return Message
     */
    public function setIsRead($isRead)
    {
        $this->is_read = $isRead;

        return $this;
    }

    /**
     * Get isRead
     *
     * @return boolean
     */
    public function getIsRead()
    {
        return $this->is_read;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\MessageType $type
     *
     * @return Message
     */
    public function setType(\AppBundle\Entity\MessageType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\MessageType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return Message
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set targetUser
     *
     * @param \AppBundle\Entity\User $targetUser
     *
     * @return Message
     */
    public function setTargetUser(\AppBundle\Entity\User $targetUser = null)
    {
        $this->target_user = $targetUser;

        return $this;
    }

    /**
     * Get targetUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getTargetUser()
    {
        return $this->target_user;
    }

    /**
     * Set targetHolder
     *
     * @param \AppBundle\Entity\User $targetHolder
     *
     * @return Message
     */
    public function setTargetHolder(\AppBundle\Entity\User $targetHolder = null)
    {
        $this->target_holder = $targetHolder;

        return $this;
    }

    /**
     * Get targetHolder
     *
     * @return \AppBundle\Entity\User
     */
    public function getTargetHolder()
    {
        return $this->target_holder;
    }
}
