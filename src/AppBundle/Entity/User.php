<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 */
class User implements AdvancedUserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $token;

    /**
     * @ORM\Column(type="string", length=60, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="datetime", name="time_created")
     */
    private $timeCreated;

    private $authenticatedWithOTP;

    /**
     * @return mixed
     */
    public function getAuthenticatedWithOTP()
    {
        return $this->get('session')->get('authenticatedWithOTP');//$this->authenticatedWithOTP;
    }

    /**
     * @param mixed $authenticatedWithOTP
     */
    public function setAuthenticatedWithOTP($authenticatedWithOTP)
    {
        $this->get('session')->set('authenticatedWithOTP', true);
        $this->authenticatedWithOTP = $authenticatedWithOTP;
    }

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(name="is_enabled", type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(name="is_locked", type="boolean")
     */
    private $isLocked;

    /**
     * @ORM\OneToMany(targetEntity="UserHolderRole", mappedBy="user")
     *
     * @var UserHolderRole[]
     */
    private $holders_and_roles;

    private $active_holder_role;

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     * @return $this
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

//    /**
//     * @return array
//     */
//    public function getUnreadMessages()
//    {
//        $result = [
//            'total' => 0,
//            'notifications' => 0,
//            'news' => 0
//        ];
//        foreach ($this->messages as $message) {
//            if (!$message->getIsRead()) {
//                $result[$message->getType()->getName()]++;
//                $result['total']++;
//            }
//        }
//        return $result;
//    }

    /**
     * @return Message[]
     */
    public function getNews()
    {

        $news = [];
        foreach ($this->messages as $message) {
            if (strtolower($message->getType()->getName()) === "news") {
                $news[] = $message;
            }
        }
        return $news;
    }

    /**
     * @param Message[] $messages
     * @return $this
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;

        return $this;
    }

    /**
     * @return Message[]
     */
    public function getMessagesSent()
    {
        return $this->messages_sent;
    }

    /**
     * @param Message[] $messages_sent
     * @return $this
     */
    public function setMessagesSent($messages_sent)
    {
        $this->messages_sent = $messages_sent;

        return $this;
    }

    private function getHolderRoleById($id)
    {
        foreach ($this->holders_and_roles as $holder_role) {
            if ($holder_role->getHolder) {

            }
        }
    }

    // TODO: Need to fix active holder recognition and switching
    public function getActiveHolder()
    {
        return $this->holders_and_roles[0]->getHolder();
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getTotalCurrency($currency = 0)
    {
        if ($currency == 0) {
            foreach ($this->active_holder_role->getHolder()->getAccounts() as $account) {

            }
        }
    }


    public function getActiveHolderRole()
    {
        if ($this->active_holder_role) {
            return $this->active_holder_role;
        }
        return $this->holders_and_roles[0];
    }

    public function setActiveHolderRole(UserHolderRole $holder_role)
    {
        if ($this->id === $holder_role->getUser()->getId()) {
            $this->active_holder_role = $holder_role;
            return $this;
        }
        return false;
    }

    public function __construct()
    {
        $this->holders = [];
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getSalt()
    {
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getHolders()
    {
        return $this->holders_and_roles;
    }

    public function getRoles()
    {
        if ($this->username == 'admin' ||
            $this->username == 'dim' ||
            $this->username == 'dinozor'
        ) {
            return array('ROLE_ADMIN');
        }
        return array('ROLE_USER');
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->email,
            $this->active_holder_role
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->email,
            $this->active_holder_role
            ) = unserialize($serialized);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return User
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent security.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent security.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return !$this->isLocked;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent security.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent security.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        return $this->isEnabled;
    }

    public function isAuthenticatedWithOTP()
    {
        return $this->authenticatedWithOTP;
    }

    /**
     * Set timeCreated
     *
     * @param \DateTime $timeCreated
     *
     * @return User
     */
    public function setTimeCreated($timeCreated)
    {
        $this->timeCreated = $timeCreated;

        return $this;
    }

    /**
     * Get timeCreated
     *
     * @return \DateTime
     */
    public function getTimeCreated()
    {
        return $this->timeCreated;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     *
     * @return User
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Add holder
     *
     * @param \AppBundle\Entity\Holder $holder
     *
     * @return User
     */
    public function addHolder(\AppBundle\Entity\Holder $holder)
    {
        $this->holders[] = $holder;

        return $this;
    }

    /**
     * Remove holder
     *
     * @param \AppBundle\Entity\Holder $holder
     */
    public function removeHolder(\AppBundle\Entity\Holder $holder)
    {
        $this->holders->removeElement($holder);
    }

    /**
     * Add holdersAndRole
     *
     * @param \AppBundle\Entity\UserHolderRole $holdersAndRole
     *
     * @return User
     */
    public function addHoldersAndRole(\AppBundle\Entity\UserHolderRole $holdersAndRole)
    {
        $this->holders_and_roles[] = $holdersAndRole;

        return $this;
    }

    /**
     * Remove holdersAndRole
     *
     * @param \AppBundle\Entity\UserHolderRole $holdersAndRole
     */
    public function removeHoldersAndRole(\AppBundle\Entity\UserHolderRole $holdersAndRole)
    {
        $this->holders_and_roles->removeElement($holdersAndRole);
    }

    /**
     * Get holdersAndRoles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHoldersAndRoles()
    {
        return $this->holders_and_roles;
    }

    /**
     * Set isLocked
     *
     * @param boolean $isLocked
     *
     * @return User
     */
    public function setIsLocked($isLocked)
    {
        $this->isLocked = $isLocked;

        return $this;
    }

    /**
     * Get isLocked
     *
     * @return boolean
     */
    public function getIsLocked()
    {
        return $this->isLocked;
    }
}
