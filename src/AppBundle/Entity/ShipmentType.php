<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="shipment_type")
 */
class ShipmentType
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    /**
     * @var \DateInterval
     *
     * @ORM\Column(name="speed_min", type="dateinterval")
     */
    private $speed_min;
    /**
     * @var \DateInterval
     *
     * @ORM\Column(name="speed_min_text", type="string", length=50)
     */
    private $speed_min_text;
    /**
     * @var \DateInterval
     *
     * @ORM\Column(name="speed_max", type="dateinterval")
     */
    private $speed_max;
    /**
     * @var \DateInterval
     *
     * @ORM\Column(name="speed_max_text", type="string", length=50)
     */
    private $speed_max_text;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ShipmentType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ShipmentType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set speedMin
     *
     * @param \timespan $speedMin
     *
     * @return ShipmentType
     */
    public function setSpeedMin(\DateInterval $speedMin)
    {
        $this->speed_min = $speedMin;

        return $this;
    }

    /**
     * Get speedMin
     *
     * @return \timespan
     */
    public function getSpeedMin()
    {
        return $this->speed_min;
    }

    /**
     * Set speedMax
     *
     * @param \timespan $speedMax
     *
     * @return ShipmentType
     */
    public function setSpeedMax(\DateInterval $speedMax)
    {
        $this->speed_max = $speedMax;

        return $this;
    }

    /**
     * Get speedMax
     *
     * @return \timespan
     */
    public function getSpeedMax()
    {
        return $this->speed_max;
    }
}
