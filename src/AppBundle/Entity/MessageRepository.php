<?php

namespace AppBundle\Entity;

/**
 * MessageRepository
 *
 */
class MessageRepository extends \Doctrine\ORM\EntityRepository
{
    public function loadNotifications()
    {

    }

    public $limit = 10;
    public $offset = 0;

    public $type_news = 2;
    public $type_msg = 3;
    public $type_nt = 1;

    private function limit_offset($qb, $limit, $offset)
    {
        if ($limit>0) {
            $qb = $qb->setMaxResults($limit);
        }
        if ($offset > 0){
            $qb = $qb->setFirstResult($offset);
        }
        return $qb;
    }

    public function findMessages($user_id, $limit = 3, $offset = 0)
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.type = :type_id AND (u.author = :user_id OR u.target_user = :user_id)')
            ->setParameter('type_id', $this->type_msg)
            ->setParameter('user_id', $user_id)
            ->orderBy('u.datetime', 'DESC');

        return $this->limit_offset($qb, $limit, $offset)->getQuery()->getResult();
    }

    public function findNotifications($uid, $hid, $limit = 0, $offset = 0)
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.type = :type_id AND (u.target_holder = :holder_id OR u.target_user = :user_id)')
            ->setParameter('type_id', $this->type_nt)
            ->setParameter('user_id', $uid)
            ->setParameter('holder_id', $hid)
            ->orderBy('u.datetime', 'DESC');

        return $this->limit_offset($qb, $limit, $offset)->getQuery()->getResult();
    }

    public function findAllNews($limit = 0, $offset = 0)
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.type = :type_id')
            ->setParameter('type_id', $this->type_news)
            ->orderBy('u.datetime', 'DESC');

        return $this->limit_offset($qb, $limit, $offset)->getQuery()->getResult();
    }
}
