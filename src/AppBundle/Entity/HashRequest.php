<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="hash_requests", uniqueConstraints={@ORM\UniqueConstraint(name="code_idx", columns={"code"})})
 */
class HashRequest
{
    public function __construct()
    {
        $this->time_created = new \DateTime();
        $this->is_read = false;
        $this->is_locked = false;
        $this->is_active = true;
        $this->pin_count = 0;
    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $pin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_created", type="datetime", options={"default": 0})
     */
    private $time_created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_last_access", type="datetime", nullable=true)
     */
    private $time_last_access;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_finished", type="datetime", nullable=true)
     */
    private $time_finished;

    /**
     * @ORM\Column(name="pin_count", type="integer")
     */
    private $pin_count;

    /**
     * @ORM\Column(type="boolean", name="is_active")
     */
    private $is_active;
    /**
     * @ORM\Column(type="boolean", name="is_read")
     */
    private $is_read;
    /**
     * @ORM\Column(type="boolean", name="is_locked")
     */
    private $is_locked;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return HashRequest
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set pin
     *
     * @param string $pin
     *
     * @return HashRequest
     */
    public function setPin($pin)
    {
        $this->pin = $pin;

        return $this;
    }

    /**
     * Get pin
     *
     * @return string
     */
    public function getPin()
    {
        return $this->pin;
    }

    /**
     * Set timeCreated
     *
     * @param \DateTime $timeCreated
     *
     * @return HashRequest
     */
    public function setTimeCreated($timeCreated)
    {
        $this->time_created = $timeCreated;

        return $this;
    }

    /**
     * Get timeCreated
     *
     * @return \DateTime
     */
    public function getTimeCreated()
    {
        return $this->time_created;
    }

    /**
     * Set timeLastAccess
     *
     * @param \DateTime $timeLastAccess
     *
     * @return HashRequest
     */
    public function setTimeLastAccess($timeLastAccess)
    {
        $this->time_last_access = $timeLastAccess;

        return $this;
    }

    /**
     * Get timeLastAccess
     *
     * @return \DateTime
     */
    public function getTimeLastAccess()
    {
        return $this->time_last_access;
    }

    /**
     * Set timeFinished
     *
     * @param \DateTime $timeFinished
     *
     * @return HashRequest
     */
    public function setTimeFinished($timeFinished)
    {
        $this->time_finished = $timeFinished;

        return $this;
    }

    /**
     * Get timeFinished
     *
     * @return \DateTime
     */
    public function getTimeFinished()
    {
        return $this->time_finished;
    }

    /**
     * Set pinCount
     *
     * @param integer $pinCount
     *
     * @return HashRequest
     */
    public function setPinCount($pinCount)
    {
        $this->pin_count = $pinCount;

        return $this;
    }

    /**
     * Get pinCount
     *
     * @return integer
     */
    public function getPinCount()
    {
        return $this->pin_count;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return HashRequest
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set isRead
     *
     * @param boolean $isRead
     *
     * @return HashRequest
     */
    public function setIsRead($isRead)
    {
        $this->is_read = $isRead;

        return $this;
    }

    /**
     * Get isRead
     *
     * @return boolean
     */
    public function getIsRead()
    {
        return $this->is_read;
    }

    /**
     * Set isLocked
     *
     * @param boolean $isLocked
     *
     * @return HashRequest
     */
    public function setIsLocked($isLocked)
    {
        $this->is_locked = $isLocked;

        return $this;
    }

    /**
     * Get isLocked
     *
     * @return boolean
     */
    public function getIsLocked()
    {
        return $this->is_locked;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return HashRequest
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
