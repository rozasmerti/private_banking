<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CardController extends Controller
{
    /**
     * @Route("/cards", name="cards")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/cards/index.html.twig', [
            'debug' => $this->dump_post($request)
        ]);
    }

    /**
     * @Route("/cards/new", name="cards_new")
     */
    public function newCardAction(Request $request)
    {
        return $this->render('default/cards/new.html.twig', [
            'debug' => $this->dump_post($request)
        ]);
    }

    //TODO: remove it after finish working with
    private function dump_post(Request $request)
    {
        ob_start();
        var_dump($request->request->all());
        $debug = ob_get_contents();
        ob_end_clean();
        return $debug;
    }
}