<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountCurrency;
use AppBundle\Entity\HashRequest;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\TransactionStatus;
use AppBundle\Entity\User;
use AppBundle\Service\Exchanger;
use Base32\Base32;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/* Error codes */
define("HASH_REQUEST_ERROR_NO_ERROR", 0);
define("HASH_REQUEST_ERROR_UNKNOWN_CODE", 1);
define("HASH_REQUEST_ERROR_WRONG_PIN", 2);
define("HASH_REQUEST_ERROR_ATTACK_DETECTED", 3);
define("HASH_REQUEST_ERROR_TIMEOUT", 4);

// Constants
define("HASH_REQUEST_MAX_TIME", 30 * 60);
define("HASH_REQUEST_MAX_TRY", 3);

class APIController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/transfer/swift", name="transfer_swift")
     */
    public function checkSWIFTAction(Request $request)
    {
        return $this->json('unknown SWIFT');
    }

    public function changeActiveAccountAction(Request $request)
    {
        $account_id = null;
        $account = null;
        if ($account_id = $request->request->get('account')) {
//            $account = $this->getUser()->
        }

        return $this->json($account);
    }

    private function getActiveHolderRole()
    {
        $session = null;
        if ($session = $this->get('session') && $session->get('active_holder_role')) {

        }
        return null;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/api/exchange", name="api_exchange")
     */
    public function exchangeCurrencyAction(Request $request)
    {
        $session = $this->get('session');
        if (!$session->get('authenticatedWithOTP')) {
            return $this->json([
                'error' => '403',
                'msg' => 'Access Denied'
            ]);
        }

        $post = $request->request->all();
        if (isset($post['currency_from'])) {
            $from = $post['currency_from'];
        } else {
            return $this->json([
                'error' => '7',
                'msg' => '"From" currency is not provided'
            ]);
        }

        if (isset($post['currency_to'])) {
            $to = $post['currency_to'];
        } else {
            return $this->json([
                'error' => '7',
                'msg' => '"To currency is not provided'
            ]);
        }

        if (isset($post['amount'])) {
            $amount = $post['amount'];
        } else {
            return $this->json([
                'error' => '7',
                'msg' => 'Amount is not provided'
            ]);
        }

        /** @var Exchanger $ex */
        $ex = $this->get('app.exchanger');
        $cur = $ex->getRateID($from, $to);
        $margin = $cur->getMargin();
        $rate = $cur->getRate();

        if ($amount < 0) {
            return $this->json([
                'error' => '8',
                'msg' => 'Amount is negative'
            ]);
        } else {
            $amount = round($amount, 2);
        }

        $result_sum = round($amount * $rate * $margin, 2);

        /** @var User $user */
        $user = $this->getUser();
        /** @var Account $account */
        $account = $user->getActiveHolder()->getAccounts()[0]; // TODO: should work with $post['from_account']

        $manager = $this->getDoctrine()->getManager();
        $em = $manager->getRepository('AppBundle:AccountCurrency');
        /** @var TransactionStatus $status */
        $status = $this->getDoctrine()->getRepository('AppBundle:TransactionStatus')->find(1);
        $type = $this->getDoctrine()->getRepository('AppBundle:TransactionType')->find(1);
        /** @var AccountCurrency $account_currency_from */
        $account_currency_from = $em->find([
            'account' => $account,
            'currency' => $cur->getFrom()
        ]);
        $balance_from = $account_currency_from->getAmount() - $amount;

        if ($balance_from < 0) {
            return $this->json([
                'error' => '9',
                'msg' => 'Not enough money'
            ]);
        }

        $account_currency_from->setAmount($balance_from);

        /** @var AccountCurrency $account_currency_to */
        $account_currency_to = $em->find([
            'account' => $account,
            'currency' => $cur->getTo()
        ]);
        if ($account_currency_to) {
            $balance_to = (float)$account_currency_to->getAmount() + $result_sum;
            $account_currency_to->setAmount($balance_to);
        } else {
            $account_currency_to = new AccountCurrency();
            $account_currency_to->setAmount($result_sum);
            $account_currency_to->setAccount($account);
            $account_currency_to->setCurrency($cur->getTo());
            $balance_to = (float)$account_currency_to->getAmount();
            $manager->persist($account_currency_to);

        }

        $manager->flush();

        $transaction = new Transaction();
        $transaction->setAccount($account);

        $transaction->setAmount(-$amount);
        $transaction->setAmountCurrency($cur->getFrom());
        $transaction->setBalance($balance_from);
        $transaction->setBalanceCurrency($cur->getFrom());
        $transaction->setReference('XCH');

        $transaction->setDetails('Currency exchange
        From: ' . $amount . ' ' . $cur->getFrom()->getShortName() . '
        To: ' . $result_sum . ' ' . $cur->getTo()->getShortName() . '
        with rate ' . $rate * $margin
        );

        $transaction->setType($type);
        $transaction->setStatus($status);

        $manager->persist($transaction);

        $manager->flush();


        $transaction = new Transaction();
        $transaction->setAccount($account);

        $transaction->setAmount($result_sum);
        $transaction->setAmountCurrency($cur->getTo());
        $transaction->setBalance($balance_to);
        $transaction->setBalanceCurrency($cur->getTo());
        $transaction->setReference('XCH');

        $transaction->setDetails('Currency exchange
        From: ' . $amount . ' ' . $cur->getFrom()->getShortName() . '
        To: ' . $result_sum . ' ' . $cur->getTo()->getShortName() . '
        with rate ' . $rate * $margin
        );

        $transaction->setType($type);
        $transaction->setStatus($status);

        $manager->persist($transaction);

        $manager->flush();

        return $this->json([
            'rate' => $cur->getRate() * ($cur->getMargin() > 0 ? $cur->getMargin() : 1),
            'from' => $cur->getFrom()->getShortName(),
            'to' => $cur->getTo()->getShortName(),
            'balance_from' => $balance_from,
            'balance_to' => $balance_to
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/api/exchange/get_rate", name="api_exchange_get_rate")
     */
    public function getCurrencyRateAction(Request $request)
    {
        /** @var Exchanger $ex */
        $ex = $this->get('app.exchanger');
        $post = $request->request->all();
        if (isset($post['from'])) {
            $from = $post['from'];
        }

        if (isset($post['to'])) {
            $to = $post['to'];
        }

        $cur = $ex->getRateID($from, $to);

        return $this->json([
            'rate' => $cur->getRate() * ($cur->getMargin() > 0 ? $cur->getMargin() : 1),
            'from' => $cur->getFrom()->getShortName(),
            'to' => $cur->getTo()->getShortName()
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * TODO: Make something with error codes. Move them to table or something. This way this is bad and unstructured
     * TODO: Also add error descriptions so that there could be translation if needed (user errors mostly)
     * @Route("/api/get_hash", name="api_get_hash")
     */
    public function getUserHashAction(Request $request)
    {
        $code = $request->request->get('code');
        $pin = $request->request->get('pin');

        $em = $this->getDoctrine()->getManager();
        $hr = $em->getRepository('AppBundle:HashRequest');
        /** @var HashRequest $req */
        $req = $hr->findOneBy(['code' => $code]);

        $date = new \DateTime();

        if (!$req) {
            return $this->json([
                'error' => HASH_REQUEST_ERROR_UNKNOWN_CODE,
                'msg' => 'Code not found',
            ]);
        }
        $req->setTimeLastAccess($date);
        $em->flush();

        if (!$req->getIsActive()) {
            return $this->json([
                'error' => HASH_REQUEST_ERROR_UNKNOWN_CODE,
                'msg' => 'Code is disabled (not active)'
            ]);
        }
        if ($req->getIsLocked()) {
            return $this->json([
                'error' => HASH_REQUEST_ERROR_UNKNOWN_CODE,
                'msg' => 'Code is locked'
            ]);
        }
        // Optional error. Maybe won't be used in some scenarios
        if ($req->getIsRead()) {
            return $this->json([
                'error' => HASH_REQUEST_ERROR_UNKNOWN_CODE,
                'msg' => 'Code is already read'
            ]);
        }

        $diff = $date->getTimestamp() - $req->getTimeCreated()->getTimestamp();
        if ($diff > HASH_REQUEST_MAX_TIME) {
            $req->setTimeFinished($date);
            $req->setIsLocked(true);
            $req->setIsActive(false);
            $em->flush();
            return $this->json([
                'error' => HASH_REQUEST_ERROR_TIMEOUT,
                'msg' => 'Request timed out',
            ]);
        }

        if ($pin !== $req->getPin()) {
            $pin_count = $req->getPinCount();
            $pin_count++;
            $req->setPinCount($pin_count);
            if ($pin_count > HASH_REQUEST_MAX_TRY) {
                $req->setIsLocked(true);
                $req->setIsActive(false);
            }
            $em->flush();
            return $this->json([
                'error' => HASH_REQUEST_ERROR_ATTACK_DETECTED,
                'msg' => 'Wrong pin',
            ]);
        }

        $token = $req->getUser()->getToken();
        $hash = substr(Base32::encode($token), 0, -3);
        $req->setIsActive(false);
        $req->setIsRead(true);
        $req->setTimeFinished($date);
        $em->flush();

        return $this->json([
            'hash' => $hash,
            'period' => 30
        ]);
    }
}