<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use AppBundle\Entity\Transaction;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="main")
     * @Route("/accounts", name="accounts")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/accounts/index.html.twig');
    }

    /**
     * @Route("/change_holder", name="change_holder")
     */
    public function changeHolderAction(Request $request)
    {
        if ($holder_id = (int)$request->request->get('holder_id')) {
            foreach ($this->getUser()->getHoldersAndRoles() as $value) {
                if ($value->getHolder()->getId() === $holder_id) {
                    $this->getUser()->setActiveHolderRole($value);
                    $value->getHolder()->setName($value->getHolder()->getName() . '!');

                    return $this->json(['result' => true]);
                }
            }
        }
        return $this->json(['result' => false, 'holder_id' => $holder_id]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/statement", name="statement")
     */
    public function statementAction(Request $request)
    {
        $account = $this->getUser()->getActiveHolder()->getAccounts()[0];
        $date_from = $request->request->get('date_from');
        $date_to = $request->request->get('date_to');

        $date_from = $date_from ? new \DateTime($date_from) : new \DateTime('previous month');
        $date_to = $date_to ? new \DateTime($date_from) : null;

        return $this->render('default/statement/index.html.twig', [
            'account' => $account,
            'statements' => $this->getTransactionRepository()->loadStatements(
                $account->getId(),
                $date_from,
                $date_to,
                $request->request->get('currency')
            ),
            'from' => $date_from,
            'to' => $date_to
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/statements_filtered", name="statements_filtered")
     */
    public function statementFilter(Request $request)
    {
        $account = $this->getUser()->getActiveHolder()->getAccounts()[0];
        $date_from = $request->request->get('date_from');
        $date_to = $request->request->get('date_to');
        $currency_id = $request->request->get('currency');

        $st = $this->getTransactionRepository()->loadStatements(
            $account->getId(),
            $date_from ? new \DateTime($date_from) : new \DateTime(),
            $date_to ? new \DateTime($date_to) : null,
            $currency_id?$currency_id:null
        );

        return $this->render('default/statement/statement_filtered.html.twig', [
            'statements' => $st
        ]);
    }

    private function getTransactionRepository()
    {
        return $this->getDoctrine()->getManager()->getRepository('AppBundle:Transaction');
    }

    public function getTotalCurrency(Request $request)
    {
        if ($currency_id = $request - $request - get('currency_id') && $currency_id == 0) {
            $amount = 0;


            return $this->json([
                'currency' => EUR,
                'amount' => $amount
            ]);
        }
        return $this->json([
            'result' => false,
            'error' => 'Wrong or no currency provided'
        ]);
    }

    /**
     * //     * @Route("/mail", name="mail")
     */
    public function mailAction($name)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Hello Email')
            ->setFrom('ibank@bankinno.com')
            ->setTo('dino@inno.lc')
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'Emails/registration.html.twig', array('name' => $name)
                ), 'text/html'
            )/*
         * If you also want to include a plaintext version of the message
          ->addPart(
          $this->renderView(
          'Emails/registration.txt.twig',
          array('name' => $name)
          ),
          'text/plain'
          )
         */
        ;
        $this->get('mailer')->send($message);

        return $this->render('Emails/registration.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
        ]);
    }

}
