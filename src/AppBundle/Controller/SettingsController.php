<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use AppBundle\Entity\Country;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SettingsController extends Controller
{
    /**
     * @Route("/settings", name="settings")
     * @Route("/settings/personal", name="settings_personal")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/settings/index.html.twig');
    }

    /**
     * @Route("/settings/security", name="settings_security")
     */
    public function securityAction(Request $request)
    {
        $plainPassword = $request->request->get('old_password');
        if ($plainPassword) {
            // TODO: remake this as UserPassword ChangePassword form, SecurityAssert etc.
            /** @var User $user */
            $user = $this->getUser();
            $encoder = $this->get('security.password_encoder');
            if ($encoder->isPasswordValid($user, $plainPassword)) {
                $encoded = $encoder->encodePassword($user, $request->request->get('new_password'));
                $user->setPassword($encoded);
                $this->getDoctrine()->getManager()->flush();
            } else {
                // TODO: Need to remake this thing and error as Flash messages
                return $this->render('default/settings/security.html.twig', [
                    'error' => 'Wrong password',
                    'debug' => $this->dump_post($request)
                ]);
            }

        }
        return $this->render('default/settings/security.html.twig');
    }

    /**
     * @Route("/settings/address", name="settings_address")
     */
    public function addressAction(Request $request)
    {
        $addresses = $this->getDoctrine()->getRepository('AppBundle:UserAddress')->findBy([
            'user' => $this->getUser()
        ]);
        return $this->render('default/settings/address.html.twig', [
            'debug' => $this->dump_post($request),
            'addresses' => $addresses,
            'countries' => $this->getCountries()
        ]);
    }

    /**
     * @Route("/settings/address/add", name="settings_address_add")
     */
    public function addAddress(Request $request)
    {
        $post = $request->request->all();

        /** @var Country $country */
        $country = $this->getDoctrine()->getRepository('AppBundle:Country')->find($post['country']);

        $address = new Address();
        $address->setCountry($country);

        $address->setAddress1($post['address1']);
        $address->setAddress2($post['address2']);
        $address->setCity($post['city']);
        $address->setPostcode($post['postcode']);
        $address->setRegion($post['region']);

        return $this->json(['debug' => $this->dump_post($request)]);

        //return $this->forward('AppBundle:Settings:address');

    }

    /**
     * // TODO: should be in separate global class/service like Helper or something and accessible from twig
     * @return array
     */
    private function getCountries()
    {
        return $this->getDoctrine()->getRepository('AppBundle:Country')->findAll();
    }

    //TODO: remove it after finish working with
    private function dump_post(Request $request)
    {
        ob_start();
        var_dump($request->request->all());
        $debug = ob_get_contents();
        ob_end_clean();
        return $debug;
    }
}