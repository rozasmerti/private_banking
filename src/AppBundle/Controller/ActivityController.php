<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

//TODO: Move it to API
class ActivityController
{
    /**
     * @Route("/activity", name="activity")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/activity/index.html.twig', [
            'debug' => $this->dump_post($request)
        ]);
    }

    //TODO: remove it after finish working with
    private function dump_post(Request $request)
    {
        ob_start();
        var_dump($request->request->all());
        $debug = ob_get_contents();
        ob_end_clean();
        return $debug;
    }
}