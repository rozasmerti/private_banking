<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ApprovalListController extends Controller
{

    /**
     * @Route("/approval", name="approval")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/approval_list/index.html.twig', [
            'debug' => $this->dump_post($request)
        ]);
    }

    /**
     * @Route("/approval/history", name="approval_history")
     */
    public function historyAction(Request $request)
    {
        return $this->render('default/approval/history.html.twig', [
            'debug' => $this->dump_post($request)
        ]);
    }

    /**
     * @Route("/approval/ready", name="approval_ready")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function readyAction(Request $request)
    {
        return $this->render('default/approval_list/ready.html.twig', [
            'debug' => $this->dump_post($request)
        ]);
    }

    //TODO: remove it after finish working with
    private function dump_post(Request $request)
    {
        ob_start();
        var_dump($request->request->all());
        $debug = ob_get_contents();
        ob_end_clean();
        return $debug;
    }
}