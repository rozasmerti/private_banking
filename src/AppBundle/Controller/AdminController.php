<?php
/**
 * Created by PhpStorm.
 * User: dinozor
 * Date: 10-10-2016
 * Time: 12:10
 */

namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Entity\User;
use AppBundle\Form\MessageType as FormMessageType;
use AppBundle\Entity\MessageType;
use AppBundle\Entity\Message;
use AppBundle\Service\Exchanger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin")
     */
    public function loginAction(Request $request)
    {
        return $this->render('admin/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
        ]);
    }

    /**
     * @Route("/admin/users", name="admin_users")
     */
    public function usersAction(Request $request)
    {
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
        return $this->render('admin/user_holders.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("admin/register", name="user_registration")
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $user->setToken(uniqid(null, true));//mt_rand(1000, 9999));

            $user->setTimeCreated(new \DateTime());
            $user->setIsEnabled(false);
            $user->setIsActive(false);
            $user->setIsLocked(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('admin_users');
        }

        return $this->render(
            'admin/register.html.twig',
            array('form' => $form->createView())
        );
    }
        /**
     * @Route("admin/send_message", name="send_message")
     */
    public function sendMessageAction(Request $request)
    {
        $message = new Message();
        $form = $this->createForm(FormMessageType::class, $message);
        $messArr=$request->get('message');

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
 
            $message
                ->setIsRead(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();

            return $this->redirectToRoute('admin_users');
        }

        return $this->render(
            'admin/send_message.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/test", name="test")
     */
    public function testAction(Request $request)
    {
        /** @var Exchanger $ex */
        $ex = $this->get('app.exchanger');


        echo $ex->getRateID(1, 2)->getRate() . '!!!';
        //$this->dump_post($ex->getRateID(1, 2));
        echo $ex->getRateID(2, 1)->getRate();
        return $this->render('admin/empty.html.twig', [
            //'debug' => $this->dump_post($ex->getRateID(1, 2))
        ]);
    }

    //TODO: remove it after finish working with
    private function dump_post($request)
    {
        ob_start();
        var_dump($request);
        $debug = ob_get_contents();
        ob_end_clean();
        return $debug;
    }
}
