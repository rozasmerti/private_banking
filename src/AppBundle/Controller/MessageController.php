<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Message;
use AppBundle\Entity\MessageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

class MessageController extends Controller
{
    private function getMessageRepository()
    {
        return $this->getDoctrine()->getManager()->getRepository('AppBundle:Message');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/messages", name="messages")
     */
    public function messagesAction()
    {
        $uid = $this->getUser()->getId();
        return $this->render('default/messages/messages.html.twig', [
            'title' => 'News',
            'messages' => $this->getMessageRepository()->findMessages($uid)
        ]);
    }

    /**
     * @Route("/messages/new", name="messages_new")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newMessageAction(Request $request)
    {
        if (($subject = $request->request->get('subject')) &&
            ($text = $request->request->get('message'))
        ) {
            $type = $this->getDoctrine()->getRepository('AppBundle:MessageType')->find(3);
            $message = new Message();
            $message->setText($text)
                ->setType($type)
                ->setSubject($subject)
                ->setAuthor($this->getUser())
                ->setDatetime(new \DateTime())
                ->setIsRead(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();
            return $this->messagesAction();
        }

        return $this->render('default/messages/new.html.twig', ['title' => 'Messages']);
    }

//    public function registerAction(Request $request)
//    {
//        $user = new User();
//        $form = $this->createForm(UserType::class, $user);
//
//        $form->handleRequest($request);
//        if ($form->isSubmitted() && $form->isValid()) {
//
//            $password = $this->get('security.password_encoder')
//                ->encodePassword($user, $user->getPassword());
//            $user->setPassword($password);
//            $user->setToken(uniqid(null, true));//mt_rand(1000, 9999));
//
//            $user->setTimeCreated(new \DateTime());
//            $user->setIsEnabled(false);
//            $user->setIsActive(false);
//
//            $em = $this->getDoctrine()->getManager();
//            $em->persist($user);
//            $em->flush();
//
//            return $this->redirectToRoute('admin_users');
//        }
//
//        return $this->render(
//            'admin/register.html.twig',
//            array('form' => $form->createView())
//        );
//    }

    /**
     * @Route("/messages/news", name="messages_news")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newsAction()
    {
        return $this->render('default/messages/news.html.twig', [
            'title' => 'News',
            'news' => $this->getMessageRepository()->findAllNews()
        ]);
    }

    /**
     * @Route("/messages/notifications", name="messages_notifications")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function notificationsAction()
    {
        $uid = $this->getUser()->getId();
        $hid = $this->getUser()->getActiveHolder()->getId();

        return $this->render('default/messages/notifications.html.twig', [
            'title' => 'Notifications',
            'notifications' => $this->getMessageRepository()->findNotifications($uid, $hid)
        ]);
    }
}