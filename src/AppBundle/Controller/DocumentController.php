<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DocumentController extends Controller
{
    /**
     * @Route("/documents", name="documents")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('default/documents/index.html.twig');
    }

    /**
     * @Route("/documents/price_list", name="price_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function priceListAction()
    {
        return $this->render('default/documents/price_list.html.twig');
    }

    /**
     * @Route("/documents/terms_and_conditions", name="terms_and_conditions")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function termsAndConditionsAction()
    {
        return $this->render('default/documents/terms_and_conditions.html.twig');
    }

    /**
     * @Route("/documents/other", name="other_documents")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function otherAction()
    {
        return $this->render('default/documents/other_documents.html.twig');
    }

    /**
     * @Route("/documents/faq", name="faq")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function faqAction()
    {
        return $this->render('default/documents/index.html.twig');
    }
}