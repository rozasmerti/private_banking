<?php

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

class PrintController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/print/account_details", name="print_account_details")
     */
    public function accountDetailsAction()
    {
        $account = $this->getUser()->getActiveHolderRole()->getHolder()->getAccounts()[0];
        return $this->render('print/index.html.twig', [
            'account' => $account,
            'template' => 'default/accounts/details_pop_up.html.twig'
        ]);
    }
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/print/statement", name="print_statement")
     */
    public function statementAction()
    {
        $account = $this->getUser()->getActiveHolderRole()->getHolder()->getAccounts()[0];
        return $this->render('print/index.html.twig', [
            'account' => $account,
            'template' => 'default/statement/details_pop_up.html.twig'
        ]);
    }
}