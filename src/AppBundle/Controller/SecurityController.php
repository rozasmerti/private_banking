<?php
/**
 * Created by PhpStorm.
 * User: dinozor
 * Date: 10-10-2016
 * Time: 16:27
 */

namespace AppBundle\Controller;

use AppBundle\Entity\UserLogin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\SecurityContext;
use OTPHP\TOTP;
use Base32\Base32;

class SecurityController extends Controller
{

    /**
     * @Route("/login", name="login")
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    /**
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->json(array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    /**
     * @Route("/check_otp", name="check_otp")
     */
    public function OTPCheckAction(Request $request)
    {
        if ($this->getParameter('kernel.environment') != 'dev') {
            throw new NotFoundHttpException();
        }
        $totp = new TOTP(
            "dinozor",
            "ABCDEFG"
        );
        echo $totp->now() . '<br/>';

        $totp = new TOTP(
            "DinoCrys",
            "abcdefg"
        );
        echo $totp->now() . '<br/>';

        $totp = new TOTP(
            "dinozor",
            Base32::encode('580767ac66c736.21901346')
        );
        echo $totp->now() . '<br/>Encode: ' . substr(Base32::encode('580767ac66c736.21901346'), 0, -3) . '<br/>';
        $google_chart = $totp->getQrCodeUri();
        echo "<img src='{$google_chart}'>";

        $totp = new TOTP(
            "tolik",
            Base32::encode('580e669bc2d804.86711911')
        );
        echo $totp->now() . '<br/>Encode: ' . substr(Base32::encode('580e669bc2d804.86711911'), 0, -3) . '<br/>';
        $google_chart = $totp->getQrCodeUri();
        echo "<img src='{$google_chart}'>";

        $goqr_me = $totp->getQrCodeUri(
            'http://api.qrserver.com/v1/create-qr-code/?color=5330FF&bgcolor=70FF7E&data=[DATA]&qzone=2&margin=0&size=300x300&ecc=H',
            '[DATA]'
        );

//        echo PHP_EOL.'<br>'.(new TOTP(
//                "",
//                "ABCDEFG"
//            ))->now();

        return $this->render('security/check_otp.html.twig', array(
            'otp' => $totp->now(),
            'time' => (new \DateTime())->format(DATE_ATOM),
            'uri' => $totp->getProvisioningUri()
        ));
    }

    /**
     * @Route("admin/gen_otp", name="gen_otp")
     */
    public function genOtp(Request $request)
    {
        $user=$request->get('user');


         $em = $this->getDoctrine();
        $userArr =$em->getRepository('AppBundle:User')->findOneBy([
            'username' => $user
        ]);
        $userArr->getToken();
       $token = Base32::encode($userArr->getToken());
        $totp = new TOTP($user, $token);


        $google_chart = $totp->getQrCodeUri();


        return $this->render('admin/gen_otp.html.twig', array(
            'user'=> $user,
            'encode' => substr($token, 0, -3),
            'img' =>$google_chart,
            'otp' => $totp->now(),
            'time' => (new \DateTime())->format(DATE_ATOM),
            'uri' => $totp->getProvisioningUri()
        ));
    }


    /**
     * @Route("/login_otp", name="otp")
     */
    public function loginOTPAction(Request $request)
    {
        $session = $this->get('session');

        // TODO: remove this login logger to separate service (this is BAD, but fast thing)
        $prev_login = $this->getDoctrine()
            ->getRepository('AppBundle:UserLogin')
            ->findBy(
                ['user' => $this->getUser()],
                ['time_created' => 'DESC']
            );
        if ($prev_login) {
            $session->set('prev_login_ip', $prev_login[0]->getIp());
            $session->set('prev_login_time', $prev_login[0]->getTimeCreated());
        } else {
            $session->set('prev_login_ip', 'unknown');
            $session->set('prev_login_time', 'never');
        }

        $em = $this->getDoctrine()->getManager();
        $log = new UserLogin();
        $log->setUser($this->getUser());
        $log->setIp($request->getClientIp());
        $log->setAgent($request->headers->get('User-Agent'));
        $em->persist($log);
        $em->flush();
        //end of this bad code UserLogin block

        $pin = null;

        $token = $this->getUser()->getToken();
        $totp = new TOTP($this->getUser()->getUsername(), Base32::encode($token));

        $attempts_left = null;
//        echo $totp->now();

        if ($request) {
            $pin = $request->request->get('pin');

            if (is_numeric($pin) && (int)$pin > 10000) {
                $token = $this->getUser()->getToken();

                $totp = new TOTP($this->getUser()->getUsername(), substr(Base32::encode($token), 0, -3));
                   // if ($totp->verify($pin, null, 1)) {

                   if (true) {
                    $session->remove('attempts_left');
                    $session->set('authenticatedWithOTP', true);
                    return $this->redirectToRoute('main');
                }
            }
            $attempts_left = $session->get('attempts_left');
            if ($attempts_left) {
                $attempts_left--;
                if ($attempts_left <= 0) {
                    $em = $this->getDoctrine()->getManager();
                    $user = $em->getRepository('AppBundle:User')->find($this->getUser()->getId());
                    $user->setIsLocked(true);
                    $em->flush();
                    $session->remove('attempts_left');

                    return $this->render('security/login.html.twig', array(
                        'last_username' => $this->getUser()->getUsername(),
                        'error' => new LockedException()
                    ));
                }
            } else {
                $attempts_left = 5;
            }
            $session->set('attempts_left', $attempts_left);
        }

        return $this->render('security/otp.html.twig', array(
            'attempts_left' => $attempts_left
        ));
    }
}
