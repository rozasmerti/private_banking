<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Account;
use AppBundle\Entity\AccountCurrency;
use AppBundle\Entity\Currency;
use AppBundle\Entity\ShipmentType;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\TransactionStatus;
use AppBundle\Entity\Transfer;
use AppBundle\Entity\User;
use Base32\Base32;
use OTPHP\TOTP;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class TransferController extends Controller
{
    /**
     * @Route("/transfer", name="transfer")
     */
    public function indexAction(Request $request)
    {
        return $this->render('@transfer/index.html.twig');
    }

    /**
     * @Route("/transfer/international", name="transfer_international")
     */
    public function internationalAction(Request $request)
    {
        return $this->render('@transfer/international.html.twig', [
            'currencies' => $this->getCurrencies(),
            'countries' => $this->getCountries()
        ]);
    }

    /**
     * @Route("/transfer/own", name="transfer_own")
     */
    public function ownAction(Request $request)
    {
        return $this->render('@transfer/own.html.twig', [
            'currencies' => $this->getCurrencies(),
            'countries' => $this->getCountries()
        ]);
    }

    /**
     * @Route("/transfer/local", name="transfer_local")
     */
    public function localAction(Request $request)
    {
        return $this->render('@transfer/local.html.twig', [
            'currencies' => $this->getCurrencies(),
            'countries' => $this->getCountries()
        ]);
    }

    /**
     * @Route("/transfer/make", name="transfer_make")
     */
    public function makeAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        /** @var Transfer $transfer */
        $transfer = $em->getRepository('AppBundle:Transfer')->find($request->request->get('transfer_id'));

        if (!$transfer) {
            return $this->render('@transfer/verify.html.twig', [
                'error' => 11,
                'transfer' => $transfer,
                'msg' => 'Transfer not found'
            ]);
        }

        $pin = null;
        $token = $this->getUser()->getToken();
        $totp = new TOTP($this->getUser()->getUsername(), Base32::encode($token));

        $pin = $request->request->get('pin');

        if (is_numeric($pin) && (int)$pin > 10000) {
            $token = $this->getUser()->getToken();
            $totp = new TOTP($this->getUser()->getUsername(), substr(Base32::encode($token), 0, -3));
            if ($totp->verify($pin, null, 1)) {
            //if (true) {
                return $this->render('@transfer/verify.html.twig', [
                    'error' => 10,
                    'transfer' => $transfer,
                    'msg' => 'wrong PIN'
                ]);
            }
        } else {

            return $this->render('@transfer/verify.html.twig', [
                'transfer' => $transfer,
                'error' => 10,
                'msg' => 'wrong PIN'
            ]);
        }
        $status = $em->getRepository('AppBundle:TransactionStatus')->findOneBy([
            'name' => 'verified'
        ]);
        $transfer->setStatus($status);
        $transfer->setTimeLastEdited(new \DateTime());
        $em->flush();

        return $this->render('@transfer/success.html.twig', [
            'transfer' => $transfer,
            'status' => 0,
            'msg' => 'Transfer verified'
        ]);
    }

    /**
     * @Route("/transfer/verify", name="transfer_verify")
     */
    public function verifyAction(Request $request)
    {
        // TODO: Add some unique token check to add transfer only one (user can refresh browser)
        $rq = $request->request;
        $em = $this->getDoctrine()->getManager();
        $status = $em->getRepository('AppBundle:TransactionStatus')->findOneBy([
            'name' => 'unverified'
        ]);
        /** @var User $user */
        $user = $this->getUser();

        /** @var ShipmentType $shipment */
        $shipment = $em->getRepository('AppBundle:ShipmentType')->findOneBy([
            'name' => $rq->get('shipment_type')
        ]);

        $transfer = new Transfer();
        $transfer->setStatus($status);
        $account = $user->getActiveHolder()->getAccounts()[0];
        $transfer->setToAccount($rq->get('account'));

        $transfer->setInitiator($this->getUser());
        $transfer->setCurrency($em->getRepository('AppBundle:Currency')->find($rq->get('currency')));
        $transfer->setAmount($rq->get('amount'));
        $transfer->setFrom($account);
        $transfer->setType($em->getRepository('AppBundle:TransferType')->findOneBy([
            'name' => $rq->get('transfer_type')
        ]));

        switch ($rq->get('transfer_type')) {
            case 'international':
                $country = $em->getRepository('AppBundle:Country')->find($rq->get('country'));
                $transfer->setCountry($country);
                $transfer->setSwift($rq->get('swift'));
            case 'local':
                //$transfer->setName (beneficiary Name) TODO: add it to Entity (missing now)
                $transfer->setShipment($shipment);
                $transfer->setComment($rq->get('purpose'));
                $transfer->setTimeSchedule(new \DateTime($rq->get('date')));
                $transfer->setToName($rq->get('beneficiary_name'));
                $transfer->setFee($this->recountFee($rq->get('fee')));
            //TODO: add fee type
            case 'own':
                break;
            default:
                break;
        }

        $em->persist($transfer);
        $em->flush();
        $transfer->setInitiator($this->getUser());
        $em->flush();


        return $this->render('@transfer/verify.html.twig', [
            'debug' => $this->dump_post($request),
            'transfer' => $transfer,
        ]);
    }

    private function recountFee($data)
    {
        return 0.0;
    }

    // TODO: Remove to separate controller (how it got here?)
    /**
     * @Route("/exchange", name="exchange")
     */
    public function exchangeAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var Account $account */
        $account = $user->getActiveHolder()->getAccounts()[0];

        $cur1 = $account->getCurrencies()[0]->getCurrency()->getId();
        $max1 = $account->getCurrencies()[0]->getAmount();
        if ($account->getCurrencies()->count() > 1){
            $cur2 = $account->getCurrencies()[1]->getCurrency()->getId();
            foreach ($account->getCurrencies() as $currency) {
                /* @var $currency AccountCurrency */
                if ($max1 < (float)$currency->getAmount()) {
                    $cur2 = $cur1;
                    $max1 = (float)$currency->getAmount();
                    $cur1 = $currency->getCurrency()->getId();
                }
            }
        } else {
            $cur2 = $cur1==1?2:1;
        }

        return $this->render('@exchange/index.html.twig', [
            'post' => $request->request->all(),
            'active_cur' => $cur1,
            'active_cur2' => $cur2,
            'currencies' => $this->getCurrencies()
        ]);
    }

    //TODO: remove it after finish working with
    private function dump_post(Request $request)
    {
        ob_start();
        var_dump($request->request->all());
        $debug = ob_get_contents();
        ob_end_clean();
        return $debug;
    }

    private function getCurrencies()
    {
        return $this->getDoctrine()->getRepository('AppBundle:Currency')->findAll();
    }

    private function getCountries()
    {
        return $this->getDoctrine()->getRepository('AppBundle:Country')->findAll();
    }

    private function createTransfer($data)
    {
        if (!isset($data['type'])) {
            return null;
        }

        $doctrine = $this->getDoctrine();

        /** @var $user User */
        $user = $this->getUser();

        $user->getActiveHolder(); //account id

        $transfer = new Transfer();
        $currency = new Currency();

        $shipment = $doctrine->getRepository('AppBundle:ShipmentType')->find($data['shipment']);

        //TODO: make to take out actual current account
        $account = $account = $this->getUser()->getActiveHolder()->getAccounts()[0];
        $transfer->setFrom($account);
        $transfer->setToAccount($data['string']);

        $transfer->setInitiator($user);
        $transfer->setCurrency($currency);
        $transfer->setAmount($data['amount']);
        $transfer->setFrom($account);

        switch ($data['type']) {
            case 'international':
                $country = $doctrine->getRepository('AppBundle:Country')->find($data['country']);
                $transfer->setCountry($country);
                $transfer->setSwift($data['swift']);
            case 'local':
                //$transfer->setName (beneficiary Name) TODO: add it to Entity (missing now)
                $transfer->setShipment($shipment);
                $transfer->setComment($data['comment']);
                $transfer->setTimeSchedule($data['date']);
                $transfer->setFee($data['fee']);
            //TODO: add fee type
            case 'own':
                break;
            default:
                break;
        }
        $transfer->setAmount($data['amount']);

        return $transfer;
    }

//    /**
//     * @Route("/api/exchange/", name="api_exchange")
//     */
//    public function exchangeVerifyAction(Request $request)
//    {
//        return $this->json([
//            'debug' => $this->dump_post($request)
//        ]);
//    }
}