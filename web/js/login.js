$(document).ready(function () {
    $("a#language-active-link").click(function () {
        return false;
    });

    /* next */
    $('input').keypress(function (e) {
        if (e.which == 13) {
            $('a#next-button').click();
            $('#login-button').click();
            return false;
        }
    });
    $('#login-button').click(function (event) {
        event.preventDefault();
        $('form').submit();
        return false;
    });
    $('a#next-button').click(function (event) {
        event.preventDefault();

        var bl_client_id = $('input[name=client_id]');
        var bl_pin = $('input[name=pin]');

        var client_id = bl_client_id.val();
        var pin = bl_pin.val();

        var e = 0;

        //client_id
        if ($.trim(client_id) == "") {
            bl_client_id.parent().addClass("form-error");
            e = 1;
        }
        else {
            bl_client_id.parent().removeClass("form-error");
            //bl_client_id.prop('readonly', true);
        }

        //pin
        if ($.trim(pin) == "") {
            bl_pin.parent().addClass("form-error");
            e = 1;
        }
        else {
            bl_pin.parent().removeClass("form-error");
            //bl_pin.prop('readonly', true);
        }

        if (!e) {
            $('div#security-failed').hide();
            $('form').submit();
        }
        else
            $('div#security-failed').show();

        return false;
    });
    /* next */

    /* One Time Password */
    $('input[name=pin]').on("keyup", function (event) {
        var pass = $(this).val();

        if ($.trim(pass) != "")
            $('a#login-button').removeClass('login-button-disabled');
        else
            $('a#login-button').addClass('login-button-disabled');
    });
    /* One Time Password */

    /* Remember me */
    $('a#security-button').click(function () {
        if ($(this).hasClass('login-button-disabled')) {
            return false;
        }
    });
    /* Remember me */

    /* Remember me */
    $('div.checkbox').click(function () {
        if ($(this).hasClass('unchecked')) {
            $(this).addClass('checked');
            $(this).removeClass('unchecked');
            $("input[name=remember]").prop('checked', true);
        }
        else {
            $(this).addClass('unchecked');
            $(this).removeClass('checked');
            $("input[name=remember]").prop('checked', false);
        }

        return false;
    });
    /* Remember me */
});