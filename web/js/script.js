/* $(window).resize(function(){
 function format (item) {
 return $(item.element).html();
 }
 $('select.select-green').select2({
 theme: "green-theme",
 formatResult: format,
 formatSelection: format,
 escapeMarkup: function (text) { return text; },
 });

 $('select.currency-select').select2({
 theme: "green-theme2"
 });

 $('select.simple-select').select2({
 theme: "green-theme2",
 minimumResultsForSearch: Infinity
 });


 $('select.form-select').select2({
 theme: "green-theme3"
 });

 $('select.simple-select2').select2({
 theme: "green-theme3",
 minimumResultsForSearch: Infinity
 });

 $("select").on("select2:open", function(event) {
 $('input.select2-search__field').attr('placeholder', 'SEARCH');
 });
 }); */

$(document).ready(function () {
    function format(item) {
        return $(item.element).html();
    }

    $('select.select-green').select2({
        theme: "green-theme",
        formatResult: format,
        formatSelection: format,
        escapeMarkup: function (text) {
            return text;
        },
    });

    $('select.simple-select2').select2({
        theme: "green-theme3",
        minimumResultsForSearch: Infinity
    });


    $('select.currency-select').select2({
        theme: "green-theme2"
    });

    $('select.simple-select').select2({
        theme: "green-theme2",
        minimumResultsForSearch: Infinity
    });


    $('select.form-select').select2({
        theme: "green-theme3"
    });


    $("select").on("select2:open", function (event) {
        $('input.select2-search__field').attr('placeholder', 'SEARCH');
    });

    $("#select-company").off("change").on("change", function (e) {
        //console.log(this.options[e.target.selectedIndex].value, e.target.selectedIndex);
        $.post("/change_holder", {
            'holder_name': $(this).text(),
            'holder_id': $(this).val()
        }, function (data) {
            location.reload();
        })
    });

    $("#send-new-message-button").click(function (e) {
        $('form').submit();
        return false;
    });


    var statement_currency_id = 'all';
    var refresh_statements = function () {
        var statements = $('#statements_list');
        var to_post = {
            'date_from': $('input[name=from]').val(),
            'date_to': $('input[name=to]').val()
        };

        if (statement_currency_id != 'all') {
            to_post.currency = statement_currency_id;
        }

        statements.html('Searching ... ');

        $jqxhr = $.post('/statements_filtered', to_post)
            .done(function (data) {
                statements.html(data);
            })
            .fail(function () {
                statements.html('Network connection error. Please try again later or contact your relationship manager.');
            });

    };
    $('a.ch-currency').click(function () {
        $('a#cur_' + statement_currency_id).removeClass('green-button4-active');
        statement_currency_id = $(this).attr('id').split('_')[1];
        $(this).addClass('green-button4-active');
        refresh_statements();
        return false;
    });
    $('input[name=from]').on('change', function () {
        refresh_statements();
    });
    $('input[name=to]').on('change', function () {
        refresh_statements();
    });

    $('a.tooltip.disable,.tooltip.disable a').click(function () {
        return false;
    });

    /* payment table */
    $('tr.payment-table-subtable').hover(function () {
            $(this).addClass('payment-tr-hover');
        },
        function () {
            $(this).removeClass('payment-tr-hover');
        });

    $('tr.payment-table-subtable').click(function () {

        var data = $(this).attr("id").split("-");

        if (!$(this).hasClass('payment-table-subtable-open')) {
            $(this).addClass('payment-table-subtable-open')
            $("tr.subtable-" + data[1]).show();
        }
        else {
            $(this).removeClass('payment-table-subtable-open')
            $("tr.subtable-" + data[1]).hide();
        }
    });

    $('tr.payment-currency-table-data').hover(function () {
            $(this).addClass('payment-currency-tr-hover');
        },
        function () {
            $(this).removeClass('payment-currency-tr-hover');
        });

    /* payment table */


    /* click on mask */
    $('#mask').click(function () {
        $('div#mask').hide();
        $('div.popup-div').hide();
    });
    /* click on mask */

    /* click on mask2 */
    $('#mask2').click(function () {
        $('div#mask2').hide();
        $('div.popup-div2').hide();
        $("body").css("overflow", "auto");
        $("div#footer-bottom-menu-mobile").find("a.chat-icon").removeClass("chat-icon-active");
		$(".account-details").find(".printWrap").removeClass("active");
    });
    /* click on mask2 */

    /* click on close button */
    $('a.close-popup-div2').click(function () {
        $('div#mask2').hide();
        $('div.popup-div2').hide();
        return false;
    });
    /* click on close button */

    /* tooltip */
    $('a.tooltip-link').hover(function () {
            if (!$("span.tooltip-mobile").is(":visible")) {
                $(this).find("span.tooltip").show();
            }
        },
        function () {
            if (!$("span.tooltip-mobile").is(":visible")) {
                $(this).find("span.tooltip").hide();
            }
        });
    /* tooltip */

    function checkDateInput() {
        var input = document.createElement('input');
        input.setAttribute('type', 'date');

        var notADateValue = 'not-a-date';
        input.setAttribute('value', notADateValue);

        return (input.value !== notADateValue);
    }

    function Check_Version() {
        var rv = -1; // Return value assumes failure.

        if (navigator.appName == 'Microsoft Internet Explorer') {

            var ua = navigator.userAgent,
                re = new RegExp("MSIE ([0-9]{1,}[\\.0-9]{0,})");

            if (re.exec(ua) !== null) {
                rv = parseFloat(RegExp.$1);
            }
        }
        else if (navigator.appName == "Netscape") {
            /// in IE 11 the navigator.appVersion says 'trident'
            /// in Edge the navigator.appVersion does not say trident
            if (navigator.appVersion.indexOf('Trident') === -1) rv = 12;
            else rv = 11;
        }

        return rv;
    }

    /* datepicker */
    // if (!checkDateInput() || $(window).width() > 737) {
    //     if (!checkDateInput()) {
    //         $("input.date").datepicker({
    //             changeMonth: true,
    //             changeYear: true,
    //             dateFormat: "dd.mm.yy",
    //             showOn: "both",
    //             buttonImage: "/images/calendar.png",
    //             buttonImageOnly: true,
    //             buttonText: "Select date",
    //             yearRange: '1970:2016',
    //             minDate: new Date(1970, 1 - 1, 1),
    //             maxDate: '+30Y'
    //         }).each(function () {
    //             //$(this).val($.datepicker.formatDate('dd.mm.yy', new Date($(this).val())));
    //             $(this).datepicker("setDate", new Date($(this).val()));
    //         });
    //     } else {
    //         if (Check_Version() == -1) {
    //             $("input.date").datepicker({
    //                 changeMonth: true,
    //                 changeYear: true,
    //                 dateFormat: "yy-mm-dd",
    //                 showOn: "both",
    //                 buttonImage: "/images/calendar.png",
    //                 buttonImageOnly: true,
    //                 buttonText: "Select date",
    //                 yearRange: '1970:2016',
    //                 minDate: new Date(1970, 1 - 1, 1),
    //                 maxDate: '+30Y'
    //             }).each(function () {
    //                 //$(this).val($.datepicker.formatDate('dd.mm.yy', new Date($(this).val())));
    //                 $(this).datepicker("setDate", new Date($(this).val()));
    //             });
    //         }
    //     }
    // }
    /* datepicker */


    /* */
    $(document).on("click", "div.payment-table-tr-mobile", function (e) {
        if ($(this).hasClass('payment-mobile-tr-close')) {
            $(this).addClass('payment-mobile-tr-open');
            $(this).removeClass('payment-mobile-tr-close');
            $(this).parent().find("div.payment-mobile-more").slideDown();
        }
        else {
            $(this).addClass('payment-mobile-tr-close');
            $(this).removeClass('payment-mobile-tr-open');
            $(this).parent().find("div.payment-mobile-more").slideUp();
        }
    });
    /* */

    $("a.make-transfer-link,a#language-active-link,a#language-active-link-footer").click(function () {
        return false;
    });

    /* account name edit */
    $("a.account-name-link").click(function () {
        return false;
        // var a_name = $(this).html();
        // $(this).hide();
        // $(this).parent().find("div.account-name-div").find("input.account-name-i").val(a_name).show();
        // $(this).parent().find("div.account-name-div").show();
        //
        // return false;
    });
    //
    // $("input.account-name-i").blur(function () {
    //     var a_name = $(this).val();
    //
    //     if ($.trim(a_name) == "")
    //         a_name = $(this).parent().find("input[name=account-hidden-name]").val();
    //
    //
    //     $(this).parent().parent().find("div.account-name-div").hide();
    //     $(this).parent().parent().find("a.account-name-link").html(a_name).show();
    //
    // });
    //
    // $("input.account-name-i").on("keyup", function (event) {
    //     if (event.which == 13) {
    //         var a_name = $(this).val();
    //
    //         if ($.trim(a_name) == "")
    //             a_name = $(this).parent().find("input[name=account-hidden-name]").val();
    //
    //         $(this).parent().parent().find("div.account-name-div").hide();
    //         $(this).parent().parent().find("a.account-name-link").html(a_name).show();
    //     }
    // });

    /* account name edit */

    /* new payment card */
    $("a.select-card").click(function () {
        var data = $(this).attr("id").split("-");

        if (!$(this).hasClass("top-button-active2")) {
            $("a.select-card").removeClass("top-button-active2");
            $("div.new-card-item").removeClass("new-card-item-active");

            $(this).addClass("top-button-active2");
            $("div#cardi-" + data[1]).addClass("new-card-item-active");

            $("input[name=card_id]").val(data[1]);
        }

        return false;
    });
    /* new payment card */

    /* checkbox */
    $('div.checkbox').click(function () {
        if ($(this).hasClass('unchecked')) {
            $(this).addClass('checked');
            $(this).removeClass('unchecked');
            $(this).parent().find("input.input-chck").prop('checked', true);
        }
        else {
            $(this).addClass('unchecked');
            $(this).removeClass('checked');
            $(this).parent().find("input.input-chck").prop('checked', false);
        }

        return false;
    });
    /* checkbox */

    /* confirm-button */
    $('a#cards-confirm').click(function () {

        var currency_card = $('select[name=currency_card]').val();
        var shops_card = $('input[name=shops_card]').val();
        var atm_card = $('input[name=atm_card]').val();
        var name_on_card = $('input[name=name_on_card]').val();
        var delivery_method_card = $('select[name=delivery_method_card]').val();
        var country_card = $('select[name=country_card]').val();
        var city_card = $('input[name=city_card]').val();
        var address_card = $('input[name=address_card]').val();
        var contacts_card = $('input[name=contacts_card]').val();
        var fees_card = $('select[name=fees_card]').val();
        var express = $('input[name=express]:checked').val();

        var e = 0;


        //shops_card
        if ($.trim(shops_card) == "") {
            $('input[name=shops_card]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=shops_card]').parent().removeClass("form-error");
        }

        //atm_card
        if ($.trim(atm_card) == "") {
            $('input[name=atm_card]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=atm_card]').parent().removeClass("form-error");
        }

        //name_on_card
        if ($.trim(name_on_card) == "") {
            $('input[name=name_on_card]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=name_on_card]').parent().removeClass("form-error");
        }

        //city_card
        if ($.trim(city_card) == "") {
            $('input[name=city_card]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=city_card]').parent().removeClass("form-error");
        }

        //address_card
        if ($.trim(address_card) == "") {
            $('input[name=address_card]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=address_card]').parent().removeClass("form-error");
        }

        //address_card
        if ($.trim(contacts_card) == "") {
            $('input[name=contacts_card]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=contacts_card]').parent().removeClass("form-error");
        }

        if (jQuery.type(express) === "undefined") {
            $('input[name=express]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=express]').parent().removeClass("form-error");
        }


        if (!e) {
            // alert('ok');
        }

        return false;
    });
    /* confirm-button */

    /* new payment card */
    $("a#order-new-card").click(function () {

        if (!$("div#w720").is(":visible")) {
            $('div#mask2').fadeIn(1000);
            $('div#new-card-div').fadeIn(1000);
        }
        else {
            $("div#all-cards").hide();
            $("div#new-card-h").show();
            $("div#new-card-div").show();
        }

        return false;
    });
    /* new payment card */

    /* message hover*/
    $('div.message-item').hover(function () {
            $(this).addClass("message-item-hover");
        },
        function () {
            $(this).removeClass("message-item-hover");
        });
    /* message click*/

    /* message show */
    $('div.message-item').click(function () {

        if ($("div#w320").is(":visible")) {
            var message_text = $(this).find("div.message-text");

            if (message_text.is(":visible")) {
                message_text.slideUp();
            }
            else {
                message_text.slideDown();
            }
        }

        return false;
    });
    /* message show */

    /* search-payment */
    $('input[name=search-payment]').click(function () {
        var sval = $(this).val();

        if ($.trim(sval) == "Search") {
            $(this).val("");
        }

        return false;
    });
    /* search-payment */

    /* loan credit cards - confirm-button 2 */
    $('a#loans-credit-cards-submit').click(function () {

        var credit_limit_card = $('input[name=credit_limit_card]').val();
        var security_deposit_card = $('input[name=security_deposit_card]').val();
        var currency_card = $('select[name=currency_card]').val();
        var shops_card = $('input[name=shops_card]').val();
        var atm_card = $('input[name=atm_card]').val();
        var name_on_card = $('input[name=name_on_card]').val();
        var delivery_method_card = $('select[name=delivery_method_card]').val();
        var country_card = $('select[name=country_card]').val();
        var city_card = $('input[name=city_card]').val();
        var address_card = $('input[name=address_card]').val();
        var contacts_card = $('input[name=contacts_card]').val();
        var fees_card = $('select[name=fees_card]').val();
        var express = $('input[name=express]:checked').val();

        var e = 0;

        //credit_limit_card
        if ($.trim(credit_limit_card) == "") {
            $('input[name=credit_limit_card]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=credit_limit_card]').parent().removeClass("form-error");
        }

        //security_deposit_card
        if ($.trim(security_deposit_card) == "") {
            $('input[name=security_deposit_card]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=security_deposit_card]').parent().removeClass("form-error");
        }


        //shops_card
        if ($.trim(shops_card) == "") {
            $('input[name=shops_card]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=shops_card]').parent().removeClass("form-error");
        }

        //atm_card
        if ($.trim(atm_card) == "") {
            $('input[name=atm_card]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=atm_card]').parent().removeClass("form-error");
        }

        //name_on_card
        if ($.trim(name_on_card) == "") {
            $('input[name=name_on_card]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=name_on_card]').parent().removeClass("form-error");
        }

        //city_card
        if ($.trim(city_card) == "") {
            $('input[name=city_card]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=city_card]').parent().removeClass("form-error");
        }

        //address_card
        if ($.trim(address_card) == "") {
            $('input[name=address_card]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=address_card]').parent().removeClass("form-error");
        }

        //address_card
        if ($.trim(contacts_card) == "") {
            $('input[name=contacts_card]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=contacts_card]').parent().removeClass("form-error");
        }

        if (jQuery.type(express) === "undefined") {
            $('input[name=express]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=express]').parent().removeClass("form-error");
        }


        if (!e) {
            // alert('ok');
        }

        return false;
    });
    /* loan credit cards - confirm-button 2 */

    /* loan real estate - confirm-button 3 */
    $('a#loans-real-estate-submit').click(function () {

        var estate_country = $('select[name=estate_country]').val();
        var estate_city = $('input[name=estate_city]').val();
        var estate_address = $('input[name=estate_address]').val();
        var estate_property = $('select[name=estate_property]').val();
        var estate_price = $('input[name=estate_price]').val();
        var currency_estate_price = $('select[name=currency_estate_price]').val();
        var estate_amount = $('input[name=estate_amount]').val();
        var currency_estate_amount = $('select[name=currency_estate_amount]').val();
        var estate_period = $('select[name=estate_period]').val();

        var e = 0;

        //estate_city
        if ($.trim(estate_city) == "") {
            $('input[name=estate_city]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=estate_city]').parent().removeClass("form-error");
        }

        //estate_address
        if ($.trim(estate_address) == "") {
            $('input[name=estate_address]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=estate_address]').parent().removeClass("form-error");
        }

        //estate_price
        if ($.trim(estate_price) == "") {
            $('input[name=estate_price]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=estate_price]').parent().removeClass("form-error");
        }

        //estate_amount
        if ($.trim(estate_amount) == "") {
            $('input[name=estate_amount]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=estate_amount]').parent().removeClass("form-error");
        }


        if (!e) {
            // alert('ok');
        }

        return false;
    });
    /* loan real estate - confirm-button 3 */

    /* close loan form */
    $('a.cancel-loan-form').click(function () {
        if ($(this).hasClass('cancel-loan-form-closed')) {
            $(this).addClass('cancel-loan-form-opened');
            $(this).removeClass('cancel-loan-form-closed');
            $(this).parent().parent().parent().parent().parent().parent().find("div.close-loan-form").slideDown();
        }
        else {
            $(this).addClass('cancel-loan-form-closed');
            $(this).removeClass('cancel-loan-form-opened');
            $(this).parent().parent().parent().parent().parent().parent().find("div.close-loan-form").slideUp();
        }

        return false;
    });
    /* close loan form */

    /* loan list */
    $('div.slide-table-item').click(function () {
        if ($(this).hasClass('slide-table-item-closed')) {
            $(this).addClass('slide-table-item-opened');
            $(this).removeClass('slide-table-item-closed');
            $(this).parent().find("div.slide-table-rows").slideDown();
        }
        else {
            $(this).addClass('slide-table-item-closed');
            $(this).removeClass('slide-table-item-opened');
            $(this).parent().find("div.slide-table-rows").slideUp();
        }

        return false;
    });

    $('div.slide-table-item').hover(function () {
        $(this).toggleClass('slide-table-item-hover');
    });
    /* loan list */

    /* loan list mobile*/
    $('div.slide-table-col.expand-in-mobile').click(function () {

        if ($("div#w320").is(":visible")) {

            var po = $(this).parent();

            if (po.hasClass('slide-table-closed')) {
                po.addClass('slide-table-opened');
                po.removeClass('slide-table-closed');

                po.parent().find("div.close-loan-form").hide();
                po.find('a.cancel-loan-form').addClass('cancel-loan-form-closed');
                po.find('a.cancel-loan-form').removeClass('cancel-loan-form-opened');

                po.parent().find("div.hide-cols-mobile").show();
                po.find("div.cancel-contract-div").show();
                po.find("div.view-contract-div").show();
            }
            else {
                po.addClass('slide-table-closed');
                po.removeClass('slide-table-opened');

                po.parent().find("div.close-loan-form").hide();
                po.find('a.cancel-loan-form').addClass('cancel-loan-form-closed');
                po.find('a.cancel-loan-form').removeClass('cancel-loan-form-opened');

                po.parent().find("div.hide-cols-mobile").hide();
                po.find("div.cancel-contract-div").hide();
                po.find("div.view-contract-div").hide();
            }
        }


    });
    /* loan list mobile*/


    /* market hover*/
    $('div.market-item-wrapper').hover(function () {
            if (!$("div#w320").is(":visible")) {
                $(this).find("div.market-bg").addClass("market-bg-hover");
                $(this).find("div.market-percent").addClass("market-percent-hover");
                $(this).find("div.market-buttons").show();
            }
        },
        function () {
            if (!$("div#w320").is(":visible")) {
                $(this).find("div.market-bg").removeClass("market-bg-hover");
                $(this).find("div.market-percent").removeClass("market-percent-hover");
                $(this).find("div.market-buttons").hide();
            }
        }
    );
    /* market hover*/

    /* market see details */
    $("a.market-see-details").click(function () {

        $('div#mask2').fadeIn(1000);
        $(this).parent().parent().parent().find('div.market-details').fadeIn(1000);

        return false;
    });
    /* market see details */

    /* market-make-request */
    $("a.market-make-request").click(function () {

        $('div#mask2').fadeIn(1000);
        $('div#market-buy-form').fadeIn(1000);

        return false;
    });
    /* market-make-request */

    /* template_name */
    $('input[name=template_name]').click(function () {
        var sval = $(this).val();

        if ($.trim(sval) == "The name of payment") {
            $(this).val("");
        }

        return false;
    });
    /* template_name */

    /* savings-subordinated-loans-submit */
    $('a#savings-subordinated-loans-submit').click(function () {

        var amount = $('input[name=amount]').val();

        var e = 0;

        //amount
        if ($.trim(amount) == "") {
            $('input[name=amount]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=amount]').parent().removeClass("form-error");
        }

        if (!e) {

        }
        else
            return false;

    });
    /* savings-subordinated-loans-submit */

    /* left menu click */
    $('div.left-menu-section-link').click(function () {
        if ($("div#w720").is(":visible")) {
            if ($(this).parent().hasClass("left-menu-item-open")) {
                $(this).parent().find("div.left-menu-wrapper").hide();
                $(this).parent().removeClass("left-menu-item-open");
            }
            else {
                $(this).parent().find("div.left-menu-wrapper").show();
                $(this).parent().addClass("left-menu-item-open");
            }
        }
    });
    /* left menu click */


    /* savings-subordinated-loans-submit */
    $('a#savings-saving-notes-submit').click(function () {

        var amount = $('input[name=amount]').val();

        var e = 0;

        //amount
        if ($.trim(amount) == "") {
            $('input[name=amount]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=amount]').parent().removeClass("form-error");
        }

        if (!e) {

        }
        else
            return false;
    });
    /* savings-subordinated-loans-submit */

    /* savings-real-estate-bonds-submit */
    $('a#savings-real-estate-bonds-submit').click(function () {

        var amount = $('input[name=amount]').val();

        var e = 0;

        //amount
        if ($.trim(amount) == "") {
            $('input[name=amount]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=amount]').parent().removeClass("form-error");
        }

        if (!e) {

        }
        else
            return false;
    });
    /* savings-real-estate-bonds-submit */

    /* savings-safe-kids-submit */
    $('a#savings-safe-kids-submit').click(function () {

        var amount = $('input[name=amount]').val();
        var price = $('input[name=price]').val();

        var e = 0;

        //amount
        if ($.trim(amount) == "") {
            $('input[name=amount]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=amount]').parent().removeClass("form-error");
        }

        //price
        if ($.trim(price) == "") {
            $('input[name=price]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=price]').parent().removeClass("form-error");
        }

        if (!e) {

        }
        else
            return false;
    });
    /*savings-safe-kids-submit */

    /* savings-retirement-plans-submit */
    $('a#savings-retirement-plans-submit').click(function () {

        var amount = $('input[name=amount]').val();
        var price = $('input[name=price]').val();

        var e = 0;

        //amount
        if ($.trim(amount) == "") {
            $('input[name=amount]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=amount]').parent().removeClass("form-error");
        }

        //price
        if ($.trim(price) == "") {
            $('input[name=price]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=price]').parent().removeClass("form-error");
        }

        if (!e) {

        }
        else
            return false;
    });
    /* savings-retirement-plans-submit */

    /* funds */
    $('div.no-hide-cols-mobile2').click(function () {
        if ($("div#w320").is(":visible")) {
            if ($(this).hasClass('slide-table-item-closed')) {
                $(this).addClass('slide-table-item-opened');
                $(this).removeClass('slide-table-item-closed');
                $(this).parent().find("div.hide-cols-mobile2").slideDown();
            }
            else {
                $(this).addClass('slide-table-item-closed');
                $(this).removeClass('slide-table-item-opened');
                $(this).parent().find("div.hide-cols-mobile2").slideUp();
            }
        }
        return false;
    });
    /* funds */

    /* search */
    $('input[name=search-form]').click(function () {
        var sval = $(this).val();

        if ($.trim(sval) == "Search by Name") {
            $(this).val("");
        }

        return false;
    });
    /* search */

    /* buy fund */
    $("a.buy-funds").click(function () {

        $('div#mask2').fadeIn(1000);
        $('div#funds-buy-form').fadeIn(1000);

        return false;
    });
    /* buy fund */

    /* settings-personal-details-submit */
    $('a#settings-personal-details-submit').click(function () {

        var first_name = $('input[name=first_name]').val();
        var second_name = $('input[name=second_name]').val();
        var issued_by = $('input[name=issued_by]').val();


        var e = 0;

        //first_name
        if ($.trim(first_name) == "") {
            $('input[name=first_name]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=first_name]').parent().removeClass("form-error");
        }

        //second_name
        if ($.trim(second_name) == "") {
            $('input[name=second_name]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=second_name]').parent().removeClass("form-error");
        }

        //issued_by
        if ($.trim(issued_by) == "") {
            $('input[name=issued_by]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=issued_by]').parent().removeClass("form-error");
        }

        if (!e) {
            // alert('ok');
        }

        return false;
    });
    /* settings-personal-details-submit */

    /* new-address-form-open */
    $('a.add-new-address-link').click(function () {
        if ($(this).parent().parent().hasClass('new-address-form-open')) {
            $(this).parent().parent().removeClass('new-address-form-open');
        }
        else {
            $(this).parent().parent().addClass('new-address-form-open');
        }

        return false;
    });
    /* new-address-form-open */

    /* close new-address-form */
    $('a.delete-button-new').click(function () {
        if ($(this).parent().parent().hasClass('new-address-form-open')) {
            $(this).parent().parent().removeClass('new-address-form-open');
        }
        else {
            $(this).parent().parent().addClass('new-address-form-open');
        }

        return false;
    });
    /* close new-address-form */

    /* edit-address-form-open */
    $(document).on("click", "a.edit-address", function () {
        if ($(this).parent().parent().hasClass('address-form-open')) {
            $(this).parent().parent().removeClass('address-form-open');
        }
        else {
            $(this).parent().parent().addClass('address-form-open');
            $(this).hide();
            $(this).parent().find("a.delete-address").hide();
        }

        return false;
    });
    /* edit-address-form-open */

    /* save-address */
    $(document).on("click", "a.save-address", function () {

        $(this).parent().parent().removeClass('address-form-open');
        $(this).parent().parent().find("a.delete-address").show();
        $(this).parent().parent().find("a.edit-address").show();
        return false;
    });
    /* save-address */

    /* delete-button */
    $(document).on("click", "a.delete-button,a.delete-button2", function () {

        $(this).parent().parent().remove();
        return false;
    });
    /* delete-button */


    $('a.save-address-new').click(function () {

        var new_country = $('select[name=country]').val();
        var new_region = $('input[name=region]').val();
        var new_city = $('input[name=city]').val();
        var new_postcode = $('input[name=postcode]').val();
        var new_address = $('input[name=address]').val();
        var new_address2 = $('input[name=address2]').val();


        var add_new_addr = '<div class="address-item">' +
            '<div class="form2-h mobile-padding">' +
            '<div class="address-item-name">' + new_country + ', ' + new_city + ', ' + new_address + ', '+ new_address2 +'</div>' +
            '<a href="#"class="edit-address"title="edit"></a>' +
            '<a href="#"class="delete-address delete-button2"title="delete"></a>' +
            '<div class="clear-both"></div></div><div class="clear-both"></div>' +
            '<div class="form2-d2 mobile-padding"><div class="form2-1col">' +
            '<div class="button padding30"><input type="radio"name="address[2]"value="1"id="address-2-1"checked>' +
            '<label for="address-2-1"></label><span class="button-name">Tax address</span></div>' +
            '<div class="button padding30"><input type="radio"name="address[2]"value="2"id="address-2-2">' +
            '<label for="address-2-2"></label><span class="button-name">Postal address</span></div></div>' +
            '<div class="clear-both"></div></div><div class="address-form2"><div class="form2-h mobile-padding">Country</div>' +
            '<div class="form2-d2 mobile-padding"><div class="form-div2 select-green-wrapper2 height32">' +
            '<select name="country[2]"class="select-green"style="width: 100%"><option value="Spain">Spain</option>' +
            '<option value="Portugal">Portugal</option><option value="Italy">Italy</option></select></div>' +
            '</div><div class="form2-h mobile-padding">Region</div><div class="form2-d2 mobile-padding font14">' +
            '<div class="form-div grey-borders"><input type="text"name="region[2]"value="Region"></div></div>' +
            '<div class="form2-h mobile-padding">City</div><div class="form2-d2 mobile-padding font14">' +
            '<div class="form-div grey-borders"><input type="text"name="city[2]"value="Moscow"></div></div>' +
            '<div class="form2-h mobile-padding">Postcode</div><div class="form2-d2 mobile-padding font14">' +
            '<div class="form-div grey-borders"><input type="text"name="postcode[2]"value="624457"></div></div>' +
            '<div class="form2-h mobile-padding">Address 1</div><div class="form2-d2 mobile-padding font14">' +
            '<div class="form-div grey-borders"><input type="text"name="address1[2]"value="st.Lenina"></div></div>' +
            '<div class="form2-h mobile-padding">Address 2</div><div class="form2-d2 mobile-padding font14">' +
            '<div class="form-div grey-borders"><input type="text"name="address2[2]"value="st.Lenina 2"></div></div>' +
            '<a href="#"title="Save"id="address-2"class="green-button8 confirm-button5 save-address">Save</a>' +
            '<a href="#"title="delete"class="delete-address-link delete-button"><span>Delete</span></a><div class="clear-both"></div></div></div>';
        $("div#address-container").append(add_new_addr);

        $('select.select-green').select2({
            theme: "green-theme",
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (text) {
                return text;
            },
        });

        $("select").on("select2:open", function (event) {
            $('input.select2-search__field').attr('placeholder', 'SEARCH');
        });


        $(this).parent().parent().removeClass('new-address-form-open');
        return false;
    });
    /* settings-personal-details-submit */

    /* checkbox */
    $('div.checkbox2').click(function () {
        if ($(this).hasClass('unchecked2')) {
            $(this).addClass('checked2');
            $(this).removeClass('unchecked2');
            $(this).parent().find("input.input-chck").prop('checked', true);
        }
        else {
            $(this).addClass('unchecked2');
            $(this).removeClass('checked2');
            $(this).parent().find("input.input-chck").prop('checked', false);
        }
    });
    /* checkbox */


    /* settings-notifications-submit */
    $('a#settings-notifications-submit').click(function () {

        var email = $('input[name=email]').val();
        var mobile = $('input[name=mobile]').val();
        var issued_by = $('input[name=issued_by]').val();


        var e = 0;

        //email
        if ($.trim(email) == "") {
            $('input[name=email]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=email]').parent().removeClass("form-error");
        }

        //mobile
        if ($.trim(mobile) == "") {
            $('input[name=mobile]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=mobile]').parent().removeClass("form-error");
        }


        if (!e) {
            // alert('ok');
        }

        return false;
    });
    /* settings-notifications-submit */


    /* sms_code */
    $('input[name=sms_code]').click(function () {
        var sval = $(this).val();

        if ($.trim(sval) == "Code from sms") {
            $(this).val("");
        }

        return false;
    });
    /* sms_code */


    /* verify-button */
    $('a.verify-button').click(function () {

        /* 		var code_obj = $(this).parent().find('input[name=sms_code]');

         var code = code_obj.val();


         var e = 0;

         //code
         if($.trim(code) == "" || $.trim(code) == "Code from sms")
         {
         code_obj.parent().addClass("form-error");
         e=1;
         }
         else
         {
         code_obj.parent().removeClass("form-error");
         }

         if(!e)
         {
         alert('ok');
         }

         return false;*/
    });
    /* verify-button */

    /* settings-security-submit */
    $('a#settings-security-submit').click(function () {

        var user_id = $('input[name=user_id]').val();
        var old_password = $('input[name=old_password]').val();
        var new_password = $('input[name=new_password]').val();
        var confirm_new_password = $('input[name=confirm_new_password]').val();


        var e = 0;

        //user_id
        if ($.trim(user_id) == "") {
            $('input[name=user_id]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=user_id]').parent().removeClass("form-error");
        }

        //old_password
        if ($.trim(old_password) == "") {
            $('input[name=old_password]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=old_password]').parent().removeClass("form-error");
        }

        //new_password
        if ($.trim(new_password) == "") {
            $('input[name=new_password]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=new_password]').parent().removeClass("form-error");
        }

        //confirm_new_password
        if ($.trim(confirm_new_password) == "") {
            $('input[name=confirm_new_password]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=confirm_new_password]').parent().removeClass("form-error");
        }


        if (!e) {
            // alert('ok');
        }

        return false;
    });
    /* settings-security-submit */

    /* sms_code */
    $('input[name=enter_code]').click(function () {
        var sval = $(this).val();

        if ($.trim(sval) == "Enter code") {
            $(this).val("");
        }

        return false;
    });
    /* sms_code */

    //calc height for mobile
    var mobile_chat_size = $(window).height() - 39 - 40 - 48;
    if ($("div#w320").is(":visible"))
        $('div#live-chat-message-div-wrapper').css("height", mobile_chat_size + "px");


    /*scroll*/
    var scroll_y = 0;
    $('.scrollbar-inner').scrollbar({
        "showArrows": true,
        "scrolly": "advanced",
        "onUpdate": function (y) {
            scroll_y = y[0].scrollHeight;
        }
    });
    $('.scrollbar-inner').scrollTop(scroll_y);

    // $('span.select2-container--green-theme2 ul').scrollbar({
    //     "showArrows": true,
    //     "scrolly": "advanced",
    //     "onUpdate": function (y) {
    //         scroll_y = y[0].scrollHeight;
    //     }
    // });
    //$('span.select2-container--green-theme2 ul').scrollbar();



    /* chat-message */
    $('input[name=chat-message]').click(function () {
        var sval = $(this).val();

        if ($.trim(sval) == "Type message here...") {
            $(this).val("");
        }

        return false;
    });
    /* chat-message */


    /* live-chat-send */
    $('a#live-chat-send,a#live-chat-send-mobile').click(function () {

        var message = $('input[name=chat-message]').val();


        var e = 0;

        if ($.trim(message) == "" || $.trim(message) == "Type message here...") {
            e = 1;
        }

        if (!e) {
            $('input[name=chat-message]').val("Type message here...");
            var add_new_message = '<div class="live-chat-message"><div class="live-chat-photo-client"><img src="/images/photo2.jpg"></div><div class="live-chat-message-client"><div class="live-chat-message-client-header"><span class="live-chat-message-operator-header-name">You</span>/<span class="live-chat-message-operator-header-time">20:23</span></div><div class="live-chat-message-client-text">' + message + '</div></div><div class="clear-both"></div></div>';
            $("div#live-chat-message-div").append(add_new_message);
            $('.scrollbar-inner').scrollTop(scroll_y);
            //alert($('.scrollbar-inner').not('.scroll-content').height());

            if ($(this).attr("id") == "live-chat-send-mobile") {
                $(this).hide();
                $("a#live-chat-attach").show();
            }

        }

        return false;
    });
    /* live-chat-send */

    /* live-chat-send on ENTER */
    $('input[name=chat-message]').on("keydown", function (event) {
        if (event.which == 13) {
            var message = $('input[name=chat-message]').val();


            var e = 0;

            if ($.trim(message) == "" || $.trim(message) == "Type message here...") {
                e = 1;
            }

            if (!e) {
                $('input[name=chat-message]').val("");
                var add_new_message = '<div class="live-chat-message"><div class="live-chat-photo-client"><img src="/images/photo2.jpg"></div><div class="live-chat-message-client"><div class="live-chat-message-client-header"><span class="live-chat-message-operator-header-name">You</span>/<span class="live-chat-message-operator-header-time">20:23</span></div><div class="live-chat-message-client-text">' + message + '</div></div><div class="clear-both"></div></div>';
                $("div#live-chat-message-div").append(add_new_message);
                $('.scrollbar-inner').scrollTop(scroll_y);
            }
        }

    });

    /* live-chat-send on ENTER */


    /* live-chat-keyup */
    $('input[name=chat-message]').on("keyup", function (event) {
        if ($("div#w320").is(":visible")) {
            $("a#live-chat-attach").hide();
            $("a#live-chat-send-mobile").show();
        }

    });

    /* live-chat-keyup */

    /* video support */
    $(document).on("click", "a.video-button", function () {
        if ($(this).hasClass('video-support-off')) {
            $(this).addClass('video-support-on');
            $(this).removeClass('video-support-off');
            $("div#live-chat-camera").show();

            /* add message to the chat */
            var add_new_message = '<div class="live-chat-message"><div class="live-chat-photo-client"><div class="video-on"></div></div><div class="live-chat-message-client"><div class="live-chat-message-client-header"><span class="live-chat-message-operator-header-name">You</span>/<span class="live-chat-message-operator-header-time">20:23</span></div><div class="live-chat-message-client-text">Video camera ON</div></div><div class="clear-both"></div></div>';

            if ($("div#w720").is(":visible")) {
                var chatH = parseInt($('div#live-chat-message-div-wrapper').css("height")) - 145;
                $('div.scrollbar-inner').css("height", chatH + "px");
            }

            $("div#live-chat-message-div").append(add_new_message);
            $('.scrollbar-inner').scrollTop(scroll_y);
        }
        else {
            $(this).addClass('video-support-off');
            $(this).removeClass('video-support-on');
            $("div#live-chat-camera").hide();

            /* add message to the chat */
            var add_new_message = '<div class="live-chat-message"><div class="live-chat-photo-client"><div class="video-off"></div></div><div class="live-chat-message-client"><div class="live-chat-message-client-header"><span class="live-chat-message-operator-header-name">You</span>/<span class="live-chat-message-operator-header-time">20:23</span></div><div class="live-chat-message-client-text">Video camera OFF</div></div><div class="clear-both"></div></div>';

            if ($("div#w720").is(":visible")) {
                var chatH = parseInt($('div#live-chat-message-div-wrapper').css("height"));
                $('div.scrollbar-inner').css("height", chatH + "px");
            }

            $("div#live-chat-message-div").append(add_new_message);
            $('.scrollbar-inner').scrollTop(scroll_y);
        }

        return false;
    });
    /* video support */

    /* video close */
    $(document).on("click", "a.live-chat-client-camera-close", function () {

        $('a.video-button').addClass('video-support-off');
        $('a.video-button').removeClass('video-support-on');
        $("div#live-chat-camera").hide();

        /* add message to the chat */
        var add_new_message = '<div class="live-chat-message"><div class="live-chat-photo-client"><div class="video-off"></div></div><div class="live-chat-message-client"><div class="live-chat-message-client-header"><span class="live-chat-message-operator-header-name">You</span>/<span class="live-chat-message-operator-header-time">20:23</span></div><div class="live-chat-message-client-text">Video camera OFF</div></div><div class="clear-both"></div></div>';

        if ($("div#w720").is(":visible")) {
            var chatH = parseInt($('div#live-chat-message-div-wrapper').css("height"));
            $('div.scrollbar-inner').css("height", chatH + "px");
        }

        $("div#live-chat-message-div").append(add_new_message);
        $('.scrollbar-inner').scrollTop(scroll_y);

        return false;
    });
    /* video support */

    /* mic support */
    $('a.mic-button').click(function () {
        if ($(this).hasClass('mic-support-off')) {
            $(this).addClass('mic-support-on');
            $(this).removeClass('mic-support-off');

            /* add message to the chat */
            var add_new_message = '<div class="live-chat-message"><div class="live-chat-photo-client"><div class="mic-on"></div></div><div class="live-chat-message-client"><div class="live-chat-message-client-header"><span class="live-chat-message-operator-header-name">You</span>/<span class="live-chat-message-operator-header-time">20:23</span></div><div class="live-chat-message-client-text">Microphone ON</div></div><div class="clear-both"></div></div>';

            $("div#live-chat-message-div").append(add_new_message);
            $('.scrollbar-inner').scrollTop(scroll_y);
        }
        else {
            $(this).addClass('mic-support-off');
            $(this).removeClass('mic-support-on');

            /* add message to the chat */
            var add_new_message = '<div class="live-chat-message"><div class="live-chat-photo-client"><div class="mic-off"></div></div><div class="live-chat-message-client"><div class="live-chat-message-client-header"><span class="live-chat-message-operator-header-name">You</span>/<span class="live-chat-message-operator-header-time">20:23</span></div><div class="live-chat-message-client-text">Microphone OFF</div></div><div class="clear-both"></div></div>';

            $("div#live-chat-message-div").append(add_new_message);
            $('.scrollbar-inner').scrollTop(scroll_y);
        }

        return false;
    });
    /* mic support */


    $('a.live-chat-cut-down,a.live-chat-cut-down-mobile').click(function () {

        $("div#live-chat").hide();
        $("div#mask2").hide();
        $("div#footer-bottom-menu-mobile").find("a.chat-icon").removeClass("chat-icon-active");
        $("body").css("overflow", "auto");

        return false;
    });

    /* open chat */
    $("a#live-chat-button,a.chat-icon,a.live-chat-open").click(function () {
        return false;
        if (!$(this).hasClass('chat-icon-active')) {
            $("body").css("overflow", "hidden");

            $('div#mask2').fadeIn(1000);
            $("div#live-chat").fadeIn(1000);
            $("div#footer-bottom-menu-mobile").find("a.chat-icon").addClass("chat-icon-active");
        }

        return false;
    });
    /* open chat */

    /* attach file in chat */
    $("a#live-chat-attach").click(function () {
        // alert("ok");
        return false;
    });
    /* attach file in chat */

    /* verify-button */
    $('a.verify-button').click(function () {
        $("div.form2-phone-verify div.form-verify div.form-div").show();
        return false;
    });
    /* verify-button */

    /* verify-code-keyup */
    $('input[name=sms_code]').on("keyup", function (event) {
        $('a.verify-button').hide();
        $(this).prop('readonly', true);
        $('div.verify-status').show();
    });
    /* verify-code-keyup */


    /* suspend card */
    $(document).on("click", "a.suspend-card-l", function () {
        var card_id = $("input[name=select-card]").val();

        $('div#card-' + card_id).addClass("locked-card");
        $('div#card-' + card_id).removeClass("active-card");

        $('div#mask2').hide();
        $('div#card-confirm').hide();

        return false;
    });
    /* suspend card */

    /* activate card */
    $(document).on("click", "a.activate-card-l", function () {
        var card_id = $("input[name=select-card]").val();

        $('div#card-' + card_id).addClass("active-card");
        $('div#card-' + card_id).removeClass("locked-card");

        $('div#mask2').hide();
        $('div#card-confirm').hide();

        return false;
    });
    /* activate card */

    /* renew card */
    $(document).on("click", "a.renew-card-l", function () {
        var card_id = $("input[name=select-card]").val();

        // alert('ok');

        $('div#mask2').hide();
        $('div#card-confirm').hide();

        return false;
    });
    /* renew card */

    /* cards-status */
    $('a.cards-status').click(function () {

        var card_id_arr = $(this).parent().parent().attr("id").split("-");

        $("input[name=select-card]").val(card_id_arr[1]);

        if ($(this).hasClass("active-card-button")) {
            $("a.event-button").addClass('suspend-card-l');
            $("a.event-button").removeClass('activate-card-l');

            $("div.confirm-header").html("SUSPEND");
            $("div.confirm-text").html("Are you sure you want to block the card");
        }
        else {
            $("a.event-button").removeClass('suspend-card-l');
            $("a.event-button").addClass('activate-card-l');

            $("div.confirm-header").html("ACTIVATE");
            $("div.confirm-text").html("Are you sure you want to activate the card");
        }

        $("a.event-button").removeClass('renew-card-l');

        $('div#mask2').fadeIn(1000);
        $('div#card-confirm').fadeIn(1000);

        return false;
    });
    /* cards-status */

    /* cards-renew */
    $('a.cards-refresh').click(function () {

        var card_id_arr = $(this).parent().parent().attr("id").split("-");

        $("input[name=select-card]").val(card_id_arr[1]);

        $("a.event-button").addClass('renew-card-l');
        $("a.event-button").removeClass('activate-card-l');
        $("a.event-button").removeClass('suspend-card-l');

        $("div.confirm-header").html("RENEW");
        $("div.confirm-text").html("Are you sure you want to renew the card");

        $('div#mask2').fadeIn(1000);
        $('div#card-confirm').fadeIn(1000);

        return false;
    });
    /* cards-renew */

    /* Account details pop up */
    $('a.account-details-link').click(function () {
        $('div#mask2').fadeIn(1000);
        $(this).parent().find('div.account-details-popup').fadeIn(1000);

        return false;
    });
    /* Account details pop up */


    /* exchange-now-button */
    $('a#exchange-now-button').click(function () {

        var from_exchange = $('input[name=from_exchange]').val();
        var to_exchange = $('input[name=to_exchange]').val();


        var e = 0;

        //from_exchange
        if ($.trim(from_exchange) == "") {
            $('input[name=from_exchange]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=from_exchange]').parent().removeClass("form-error");
        }

        //to_exchange
        if ($.trim(to_exchange) == "") {
            $('input[name=to_exchange]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=to_exchange]').parent().removeClass("form-error");
        }

        if (!e) {
            // alert('ok');
        }

        return false;
    });
    /* exchange-now-button */

    /* checkbox sign now*/
    $('div.sign-checkbox').click(function () {

        var active_rows = 0;

        $('input[name="sign[]"]:checked').each(function (i) {
            active_rows++;
        });

        if (active_rows) {
            $('a.approval-sign').removeClass('approval-sign-disable');
        }
        else {
            $('a.approval-sign').addClass('approval-sign-disable');
        }
    });
    /* checkbox sign now*/

    /* sign now */
    $('a.approval-sign').click(function () {

        if (!$(this).hasClass('approval-sign-disable')) {
            var checked_rows = new Array();

            $('input[name="sign[]"]:checked').each(function (i) {
                checked_rows[i] = $(this).val();
            });
        }
        else
            return false;

    });
    /* sign now */

    /* sign now mobile*/
    $('a.approval-sign-mobile').click(function () {

        var checked_id = $(this).attr("id").split("-");
        //alert(checked_id[1]);
    });
    /* sign now mobile*/

    $('div.payment-table-tr-sign').hover(function () {
        $(this).toggleClass('payment-table-tr-sign-hover');
    });

    /* name="pass" */
    $("input[name=pass]").on("keyup", function (event) {

        if ($(this).val().length == 0) {
            $(this).parent().parent().find("div#ot2").removeClass('sms-ok');
            $(this).parent().parent().find("div#ot2").removeClass('sms-nok');
            $(this).parent().parent().find("div#ot2").html("type here...");
        }
        else if ($(this).val().length <= 3) {
            $(this).parent().parent().find("div#ot2").addClass('sms-nok');
            $(this).parent().parent().find("div#ot2").removeClass('sms-ok');
            $(this).parent().parent().find("div#ot2").html("Invalid");
        }
        else if ($(this).val().length > 3) {
            $(this).parent().parent().find("div#ot2").addClass('sms-ok');
            $(this).parent().parent().find("div#ot2").removeClass('sms-nok');
            $(this).parent().parent().find("div#ot2").html("Ok");
        }
    });
    /* name="pass" */

    /* sign-payment-button */
    $('a#sign-payment-button').click(function () {

        var pass = $('input[name=pass]').val();


        var e = 0;

        //pass
        if ($.trim(pass) == "") {
            $('input[name=pass]').parent().addClass("form-error");
            e = 1;
        }
        else {
            $('input[name=pass]').parent().removeClass("form-error");
        }


        if (!e) {
            // alert('ok');
        }

        return false;
    });
    /* sign-payment-button */


});